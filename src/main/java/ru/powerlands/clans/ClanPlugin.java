package ru.powerlands.clans;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import ru.powerlands.clans.service.BaseService;
import ru.powerlands.clans.service.ListenerService;
import ru.powerlands.clans.service.ManagerService;

public final class ClanPlugin extends JavaPlugin {

    private BaseService baseService;
    private ManagerService managerService;
    private ListenerService listenerService;

    @Override
    public void onEnable() {
        baseService = new BaseService(this);
        managerService = new ManagerService(this);
        listenerService = new ListenerService(this);

        managerService.load();
        baseService.load();

        //kallcode
        managerService.getLeaderBordManager().updateTopBalance();
        //kallcode

        listenerService.load();
    }

    @Override
    public void onDisable() {
        managerService.unload();
        baseService.unload();
        listenerService.unload();
    }

    public ManagerService getManagerService() {
        return managerService;
    }

    public BaseService getBaseService() {
        return baseService;
    }
}
