package ru.powerlands.clans;

public abstract class Instanced {

    private final ClanPlugin plugin;

    public Instanced(ClanPlugin plugin) {
        this.plugin = plugin;
    }

    public ClanPlugin getPlugin() {
        return plugin;
    }
}
