package ru.powerlands.clans.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import org.bukkit.entity.Player;
import ru.landsproject.api.util.Money;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.manager.ConfigManager;
import ru.powerlands.clans.manager.InviteManager;
import ru.powerlands.clans.manager.MessageManager;
import ru.powerlands.clans.model.Clan;

@CommandAlias("clan")
@SuppressWarnings("unused")
public class ClanEconomyPopulateCommand extends BaseCommand {
    @Dependency
    private ClanPlugin clanPlugin;
    @Dependency
    private ClanManager clanManager;
    @Dependency
    private ConfigManager configManager;
    @Dependency
    private MessageManager messageManager;
    @Dependency
    private InviteManager inviteManager;

    @Subcommand("money")
    @Description("{@@language.command.clan.money}")
    @Conditions("clan_member|clan_permission:name=SHOW_MONEY")
    @CommandPermission("clan.command.money")
    public void onMoneyCommand(Player player, Clan clan) {
        messageManager.sendMessageWithPrefixAndConf(player, "messages.money", configManager.getMessages(), "<size>", String.valueOf(clan.getMoney()));
    }
    @Subcommand("invset")
    @Description("{@@language.command.clan.invset}")
    @Conditions("clan_member|clan_permission:name=INVEST_MONEY")
    @CommandPermission("clan.command.invset")
    public void onInvestMoney(Player player, Clan clan, @Name("money") Integer money) throws Exception {
        if(money <= 0) return;
        if((clan.getMoney() + money) > clan.getMoneyLimit()) {
            messageManager.sendMessageWithPrefix(player, "Вы превышаете лимит казны!", configManager.getMessages());
            return;
        }
        Money moneyManager = new Money(player);
        if(!moneyManager.has(money)) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.invest.absentMoney", configManager.getMessages(), "<size>", String.valueOf(money));
            return;
        }
        clan.setMoney(clan.getMoney() + money);
        clanManager.updateClan(clan);

        moneyManager.take(money);
        messageManager.sendMessageWithPrefixAndConf(player, "messages.invest.invested", configManager.getMessages(), "<size>", String.valueOf(money));
    }

    @Subcommand("withdraw")
    @Description("{@@language.command.clan.withdraw}")
    @Conditions("clan_member|clan_permission:name=WITHDRAW_MONEY")
    @CommandPermission("clan.command.withdraw")
    public void onWithdrawMoney(Player player, Clan clan, @Name("money") Integer money) throws Exception {
        if(money <= 0) return;
        if(clan.getMoney() < money) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.withdraw.absentMoney", configManager.getMessages(), "<size>", String.valueOf(money));
            return;
        }
        clan.setMoney(clan.getMoney() - money);
        clanManager.updateClan(clan);
        Money moneyManager = new Money(player);
        moneyManager.take(money);
        messageManager.sendMessageWithPrefixAndConf(player, "messages.withdraw.withdrawed", configManager.getMessages(), "<size>", String.valueOf(money));
    }

}
