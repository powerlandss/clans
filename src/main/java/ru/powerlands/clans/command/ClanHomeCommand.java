package ru.powerlands.clans.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import org.bukkit.entity.Player;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.manager.ConfigManager;
import ru.powerlands.clans.manager.MessageManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.Home;

import java.util.List;
import java.util.stream.Collectors;

@CommandAlias("clan")
@SuppressWarnings("unused")
public class ClanHomeCommand extends BaseCommand {
    @Dependency
    private ClanPlugin clanPlugin;
    @Dependency
    private ClanManager clanManager;
    @Dependency
    private ConfigManager configManager;
    @Dependency
    private MessageManager messageManager;

    @Subcommand("sethome")
    @Description("{@@language.command.clan.sethome}")
    @Conditions("clan_member|clan_permission:name=CLAN_SETHOME")
    @CommandPermission("clan.command.sethome")
    public void onSethomeCommand(Player player, Clan clan, @Name("name") String name) {
        List<String> homeStringList = clan.getHomes().stream().map(home -> home.getName()).collect(Collectors.toList());
        if(homeStringList.contains(name)) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.sethome.already", configManager.getMessages());
            return;
        }
        if(clan.getLimitHomes() <= homeStringList.size()) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.sethome.limit", configManager.getMessages());
            return;
        }
        Home home = new Home(name, player.getWorld().getName(), player.getLocation());
        clan.getHomes().add(home);
        clanManager.updateClan(clan);
        messageManager.sendMessageWithPrefixAndConf(player, "messages.sethome.success", configManager.getMessages(), "<name>", name);
    }

    @Subcommand("delhome")
    @Description("{@@language.command.clan.delhome}")
    @Conditions("clan_member|clan_permission:name=CLAN_DELHOME")
    @CommandPermission("clan.command.delhome")
    public void onDeleteHome(Player player, Clan clan, @Name("name") String name) {
        List<String> homeStringList = clan.getHomes().stream().map(home -> home.getName()).collect(Collectors.toList());
        if(!homeStringList.contains(name)) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.delhome.absentHome", configManager.getMessages());
            return;
        }
        for(Home home : clan.getHomes()) {
            if(home.getName().equalsIgnoreCase(name)) {
                clan.getHomes().remove(home);
                clanManager.updateClan(clan);
                messageManager.sendMessageWithPrefixAndConf(player, "messages.delhome.success", configManager.getMessages(), "<name>", name);
                break;
            }
        }
    }

    @Subcommand("home")
    @Description("{@@language.command.clan.home}")
    @Conditions("clan_member")
    @CommandCompletion("@clan_homes")
    @CommandPermission("clan.command.home")
    public void onHomeCommand(Player player, Clan clan, @Name("name") String name) {
        List<String> homeStringList = clan.getHomes().stream().map(home -> home.getName()).collect(Collectors.toList());
        if(!homeStringList.contains(name)) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.home.absentHome", configManager.getMessages());
            return;
        }
        for(Home home : clan.getHomes()) {
            if(home.getName().equalsIgnoreCase(name)) {
                player.teleport(home.getLocation());
                messageManager.sendMessageWithPrefixAndConf(player, "messages.home.success", configManager.getMessages(), "<name>", home.getName());
                break;
            }
        }
    }

}