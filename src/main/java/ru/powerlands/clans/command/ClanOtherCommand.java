package ru.powerlands.clans.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import org.bukkit.entity.Player;
import ru.landsproject.api.configuration.Messager;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.manager.ConfigManager;
import ru.powerlands.clans.manager.MenuManager;
import ru.powerlands.clans.manager.MessageManager;
import ru.powerlands.clans.menu.storage.StorageMenu;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Storage;

@CommandAlias("clan")
@SuppressWarnings("unused")
public class ClanOtherCommand extends BaseCommand {
    @Dependency
    private ClanPlugin clanPlugin;
    @Dependency
    private ClanManager clanManager;
    @Dependency
    private ConfigManager configManager;
    @Dependency
    private MessageManager messageManager;
    @Dependency
    private MenuManager menuManager;

    @Subcommand("top")
    @Description("{@@language.command.clan.top}")
    @CommandPermission("clan.command.top")
    public void onTopCommand(Player player) {
        messageManager.sendMessageWithPrefix(player, Messager.parseApi(configManager.getMessages().getString("messages.top"), player), configManager.getMessages());
    }

    @Subcommand("menu")
    @Description("{@@language.command.clan.menu}")
    @CommandPermission("clan.command.menu")
    @Conditions("clan_member")
    public void onMenuCommand(Player player, Clan clan, ClanPlayer clanPlayer) {
        menuManager.getMainMenu().MAIN_MENU_INVENTORY().open(player);
    }
    @Subcommand("storage")
    @Description("{@@language.command.clan.storage}")
    @CommandPermission("clan.command.storage")
    @Conditions("clan_member")
    public void onStorageCommand(Player player, Clan clan) {
        new StorageMenu(clanPlugin, player, clan).open();
    }



}