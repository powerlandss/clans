package ru.powerlands.clans.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.landsproject.api.configuration.Messager;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.manager.ConfigManager;
import ru.powerlands.clans.manager.InviteManager;
import ru.powerlands.clans.manager.MessageManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Permission;
import ru.powerlands.clans.model.Status;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@CommandAlias("clan")
@SuppressWarnings("unused")
public class ClanPlayerPopulateCommand extends BaseCommand {
    @Dependency
    private ClanPlugin clanPlugin;
    @Dependency
    private ClanManager clanManager;
    @Dependency
    private ConfigManager configManager;
    @Dependency
    private MessageManager messageManager;
    @Dependency
    private InviteManager inviteManager;

    @Subcommand("leave")
    @Description("{@@language.command.clan.leave}")
    @Conditions("clan_member")
    @CommandPermission("clan.command.leave")
    public void onLeaveCommand(Player player, Clan clan, ClanPlayer clanPlayer) {
        if(clanPlayer.getStatus() == Status.OWNER) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.leave.owner", configManager.getMessages(), "<name>", clan.getDisplayName());
            return;
        }
        messageManager.sendMessageWithPrefixAndConf(player, "messages.leave.success", configManager.getMessages(), "<name>", clan.getDisplayName());
        clanManager.removePlayer(clanPlayer, clan);
    }
    @Subcommand("invite")
    @Description("{@@language.command.clan.invite}")
    @Conditions("clan_member")
    @CommandPermission("clan.command.invite")
    @CommandCompletion("@players")
    public void onInviteCommand(Player player, Clan clan, ClanPlayer clanPlayer, @Name("player") OnlinePlayer onlinePlayer) {
        Player rawInvitePlayer = onlinePlayer.getPlayer();

        if(clanManager.getClanPlayer(rawInvitePlayer.getName()) != null) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.alreadyInClan", configManager.getMessages(), "<player>", rawInvitePlayer.getName());
            return;
        }
        if((clan.getPlayers().size() + 1) >= clan.getLimitMembers()) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.limitMembers", configManager.getMessages(), "<size>", String.valueOf(clan.getLimitMembers()));
            return;
        }
        if(inviteManager.getWaitAccept().getIfPresent(rawInvitePlayer) != null) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.alreadyInvite", configManager.getMessages(), "<player>", rawInvitePlayer.getName());
            return;
        }
        messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.success", configManager.getMessages(), "<player>", rawInvitePlayer.getName());
        inviteManager.getWaitAccept().put(rawInvitePlayer, clan);
        messageManager.sendMessageWithPrefixAndConf(rawInvitePlayer, "messages.invite.text", configManager.getMessages(), "<player>", player.getName());
    }
    @Subcommand("accept")
    @Description("{@@language.command.clan.accept}")
    @Conditions("clan_isNotMember")
    @CommandPermission("clan.command.accept")
    public void onAcceptCommand(Player player) {
        Clan clan = inviteManager.getWaitAccept().getIfPresent(player);
        if(clan == null || clanManager.getClanByTag(clan.getTag()) == null) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.nonInvite", configManager.getMessages());
            return;
        }

        if((clan.getPlayers().size() + 1) >= clan.getLimitMembers()) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.limitMembers", configManager.getMessages(), "<size>", String.valueOf(clan.getLimitMembers()));
            return;
        }
        messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.successAccept", configManager.getMessages());
        inviteManager.getWaitAccept().invalidate(player);
        clanManager.createClanPlayer(player, clan, Status.MEMBER);
        clan.getPlayers().stream().map(Bukkit::getPlayer).filter(Objects::nonNull).collect(Collectors.toList()).forEach(onlinePlayer -> {
            messageManager.sendMessageWithPrefixAndConf(onlinePlayer, "messages.invite.notifyAll", configManager.getMessages(), "<player>", player.getName());
        });
    }
    @Subcommand("cancel")
    @Description("{@@language.command.clan.cancel}")
    @Conditions("clan_isNotMember")
    @CommandPermission("clan.command.cancel")
    public void onCancelCommand(Player player) {
        Clan clan = inviteManager.getWaitAccept().getIfPresent(player);
        if(clan == null || clanManager.getClanByTag(clan.getTag()) == null) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.nonInvite", configManager.getMessages());
            return;
        }
        messageManager.sendMessageWithPrefixAndConf(player, "messages.invite.successCancel", configManager.getMessages());
        inviteManager.getWaitAccept().invalidate(player);
    }

    @Subcommand("kick")
    @Description("{@@language.command.clan.kick}")
    @Conditions("clan_member")
    @CommandPermission("clan.command.kick")
    @CommandCompletion("@clan_members")
    public void onKickCommand(Player player, Clan clan, @Name("name") String targetPlayer) {
        ClanPlayer clanPlayer = clanManager.getClanPlayer(targetPlayer);
        if(clanPlayer == null) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.kick.non", configManager.getMessages());
            return;
        }
        if(clanManager.checkClanPermission(clanPlayer, Permission.ALL)) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.kick.owner", configManager.getMessages(), "<player>", targetPlayer);
            return;
        }
        messageManager.sendMessageWithPrefixAndConf(player, "messages.kick.success", configManager.getMessages(), "<player>", targetPlayer);
        clan.getPlayers().stream().map(Bukkit::getPlayer).filter(Objects::nonNull).collect(Collectors.toList()).forEach(onlinePlayer -> {
            messageManager.sendMessageWithPrefixAndConf(onlinePlayer, "messages.kick.kicked", configManager.getMessages(), "<player>", player.getName(), "<target>", targetPlayer);
        });
        clanManager.removePlayer(clanPlayer, clan);

    }


}