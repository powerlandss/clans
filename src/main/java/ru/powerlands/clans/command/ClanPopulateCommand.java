package ru.powerlands.clans.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.landsproject.api.configuration.Messager;
import ru.landsproject.api.util.Money;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.manager.ConfigManager;
import ru.powerlands.clans.manager.MessageManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Permission;
import ru.powerlands.clans.util.MultiUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@CommandAlias("clan")
@SuppressWarnings("unused")
public class ClanPopulateCommand extends BaseCommand {
    @Dependency
    private ClanPlugin clanPlugin;
    @Dependency
    private ClanManager clanManager;
    @Dependency
    private ConfigManager configManager;
    @Dependency
    private MessageManager messageManager;

    @Subcommand("create")
    @Description("{@@language.command.clan.create}")
    @Conditions("clan_isNotMember")
    @CommandPermission("clan.command.create")
    public void onClanCreateCommand(Player player, @Name("clan_name") String name) {
        Clan rawClan = clanManager.getClanByTag(name);
        if(rawClan != null) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.already", configManager.getMessages());
            return;
        }
        Pattern pattern = Pattern.compile("^[a-zA-Zа-яА-Я0-9]+$");
        Matcher matcher = pattern.matcher(name);
        if(!matcher.matches()) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.matches", configManager.getMessages());
            return;
        }
        if(name.contains("&") || name.contains("§")) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.color", configManager.getMessages());
            return;
        }
        if(name.length() <  configManager.getSettings().getInt("settings.create.minLength")) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.minLength", configManager.getMessages());
            return;
        }
        if(name.length() > configManager.getSettings().getInt("settings.create.maxLength")) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.maxLength", configManager.getMessages());
            return;
        }
        Money money = new Money(player);
        if(!money.has(configManager.getSettings().getDouble("settings.create.minPrice"))) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.absentMoney", configManager.getMessages());
            return;
        }
        money.take(configManager.getSettings().getDouble("settings.create.minPrice"));
        Clan clan = clanManager.createClan(player, name);
        messageManager.sendMessageWithPrefixAndConf(player, "messages.create.success", configManager.getMessages(), "<name>", name);
    }

    @Subcommand("disband")
    @Description("{@@language.command.clan.disband}")
    @Conditions("clan_owner|clan_permission:name=ALL")
    @CommandPermission("clan.command.disband")
    public void onDisbandCommand(Player player, Clan clan, ClanPlayer clanPlayer) {
        messageManager.sendMessageWithPrefixAndConf(player, "messages.disband.success", configManager.getMessages(), "<name>", clan.getDisplayName());
        clanManager.removeClan(clan);
    }

    @Subcommand("prefix")
    @Description("{@@language.command.clan.prefix}")
    @Conditions("clan_owner|clan_permission:name=ALL")
    @CommandPermission("clan.command.prefix")
    @CommandCompletion("@players")
    public void onPrefixCommand(Player player, Clan clan, ClanPlayer clanPlayer, @Name("target") String targetPlayer, @Name("prefix") String prefix) {
        prefix = Messager.color(prefix);
        if(!clan.getPlayers().contains(targetPlayer)) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.prefixPlayer.non", configManager.getMessages(), "<name>", targetPlayer);
            return;
        }
        ClanPlayer targetClanPlayer = clanManager.getClanPlayer(targetPlayer);
        if(targetClanPlayer == null) return;
        targetClanPlayer.setPrefix(prefix);
        clanManager.updateClanPlayer(targetClanPlayer);
        messageManager.sendMessageWithPrefixAndConf(player, "messages.prefixPlayer.renamed", configManager.getMessages(), "<player>", targetPlayer, "<name>", prefix);
    }
    @Subcommand("rename")
    @Description("{@@language.command.clan.rename}")
    @Conditions("clan_owner|clan_permission:name=CLAN_RENAME")
    @CommandPermission("clan.command.rename")
    public void onRenameCommand(Player player, Clan clan, @Name("name") String clanName) {
        String removedColorClanName = MultiUtil.removeColorCodes(clanName);
        if (removedColorClanName.length() > configManager.getSettings().getInt("settings.create.maxLength")) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.maxLength", configManager.getMessages());
            return;
        }
        if (removedColorClanName.length() > clan.getLengthLimit()) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.rename.maxLength", configManager.getMessages(), "<size>", String.valueOf(clan.getLengthLimit()));
            return;
        }
        List<String> colorCodes = new ArrayList<>();
        int index = 0;
        while (index < clanName.length()) {
            if (clanName.charAt(index) == '&') {
                if (index + 1 < clanName.length())
                    colorCodes.add(String.valueOf(clanName.charAt(index + 1)));
                index += 2;
                continue;
            }
            index++;
        }
        for (String color : colorCodes) {
            if (!clan.getColors().contains(color)) {
                messageManager.sendMessageWithPrefixAndConf(player, "messages.rename.absentColor", configManager.getMessages(), "<color>", color.replace("&", ""));
                return;
            }
        }

        List<Clan> firstCheck = clanManager.getClanByDisplay(clanName);
        Clan twoCheck = clanManager.getClanByTag(clanName);
        if(twoCheck != null || (firstCheck != null && !firstCheck.isEmpty())) {
            messageManager.sendMessageWithPrefixAndConf(player, "messages.create.already", configManager.getMessages());
            return;
        }
        clan.setDisplayName(clanName);
        clanManager.updateClan(clan);
        clan.getPlayers().stream().map(Bukkit::getPlayer).filter(Objects::nonNull).collect(Collectors.toList()).forEach(onlinePlayer -> {
            messageManager.sendMessageWithPrefixAndConf(onlinePlayer, "messages.rename.notifyAll", configManager.getMessages(), "<player>", player.getName(), "<name>", Messager.color(clanName));
        });
    }


}