package ru.powerlands.clans.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.manager.ConfigManager;
import ru.powerlands.clans.manager.MessageManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Permission;
import ru.powerlands.clans.model.Status;

import java.util.Objects;
import java.util.stream.Collectors;

@CommandAlias("clan")
@SuppressWarnings("unused")
public class ClanSettingCommand extends BaseCommand {
    @Dependency
    private ClanPlugin clanPlugin;
    @Dependency
    private ClanManager clanManager;
    @Dependency
    private ConfigManager configManager;
    @Dependency
    private MessageManager messageManager;  
    @Subcommand("pvp")
    @Description("{@@language.command.clan.pvp}")
    @Conditions("clan_member|clan_permission:name=PVP_TOGGLE")
    @CommandPermission("clan.command.pvp")
    public void onDisbandCommand(Player player, Clan clan, ClanPlayer clanPlayer) {
        String state;
        if(clan.getPvp()) {
            state = "enable";
        } else {
            state = "disable";
        }
        clan.setPvp(!clan.getPvp());

        clan.getPlayers().stream().map(Bukkit::getPlayer).filter(Objects::nonNull).collect(Collectors.toList()).forEach(onlinePlayer -> {
            messageManager.sendMessageWithPrefixAndConf(onlinePlayer, "messages.pvp.notifyAll", configManager.getMessages(), "<player>", player.getName(), "<state>", configManager.getMessages().getString("messages.pvp." + state));
        });

        clanManager.updateClan(clan);
    }
}