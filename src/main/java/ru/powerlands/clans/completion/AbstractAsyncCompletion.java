package ru.powerlands.clans.completion;

import co.aikar.commands.BukkitCommandCompletionContext;
import co.aikar.commands.CommandCompletions.AsyncCommandCompletionHandler;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;

public abstract class AbstractAsyncCompletion extends AbstractCompletion
        implements AsyncCommandCompletionHandler<BukkitCommandCompletionContext> {
    public AbstractAsyncCompletion(@NotNull ClanPlugin plugin) {
        super(plugin);
    }
}