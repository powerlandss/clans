package ru.powerlands.clans.completion;

import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.interfaces.IdentifiableCompletion;
import ru.powerlands.clans.manager.ClanManager;

public abstract class AbstractCompletion implements IdentifiableCompletion {

    protected final ClanPlugin plugin;
    protected final ClanManager clanManager;
    public AbstractCompletion(@NotNull ClanPlugin plugin) {
        this.plugin = plugin;
        this.clanManager = plugin.getManagerService().getClanManager();
    }
}