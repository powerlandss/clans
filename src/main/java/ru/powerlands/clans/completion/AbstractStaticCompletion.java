package ru.powerlands.clans.completion;

import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;

import java.util.Collection;

public abstract class AbstractStaticCompletion extends AbstractCompletion {

    public AbstractStaticCompletion(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @NotNull
    public abstract Collection<String> getCompletions();

}