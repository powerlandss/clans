package ru.powerlands.clans.completion;

import co.aikar.commands.BukkitCommandCompletionContext;
import co.aikar.commands.CommandCompletions.CommandCompletionHandler;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;

public abstract class AbstractSyncCompletion extends AbstractCompletion
        implements CommandCompletionHandler<BukkitCommandCompletionContext> {
    public AbstractSyncCompletion(@NotNull ClanPlugin plugin) {
        super(plugin);
    }
}