package ru.powerlands.clans.completion.model;

import co.aikar.commands.BukkitCommandCompletionContext;
import co.aikar.commands.InvalidCommandArgument;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.completion.AbstractAsyncCompletion;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;

import java.util.Collection;
import java.util.Collections;
@SuppressWarnings("unused")
public class ClanMemberCompletion extends AbstractAsyncCompletion {
    public ClanMemberCompletion(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public Collection<String> getCompletions(BukkitCommandCompletionContext context) throws InvalidCommandArgument {
        Player player = context.getPlayer();
        ClanPlayer clanPlayer = clanManager.getClanPlayer(player.getName());
        if(clanPlayer == null) return Collections.emptyList();
        Clan clan = clanManager.getClanByTag(clanPlayer.getClanTag());
        if(clan == null) return Collections.emptyList();
        return clan.getPlayers();
    }

    @Override
    public @NotNull String getId() {
        return "clan_members";
    }
}
