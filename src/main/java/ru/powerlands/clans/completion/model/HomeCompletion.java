package ru.powerlands.clans.completion.model;

import co.aikar.commands.BukkitCommandCompletionContext;
import co.aikar.commands.InvalidCommandArgument;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.completion.AbstractAsyncCompletion;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class HomeCompletion extends AbstractAsyncCompletion {
    public HomeCompletion(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public Collection<String> getCompletions(BukkitCommandCompletionContext context) throws InvalidCommandArgument {
        Player player = context.getPlayer();
        ClanPlayer clanPlayer = clanManager.getClanPlayer(player.getName());
        if(clanPlayer == null) return Collections.emptyList();
        Clan clan = clanManager.getClanByTag(clanPlayer.getClanTag());
        return clan.getHomes().stream().map(home -> home.getName()).collect(Collectors.toList());
    }

    @Override
    public @NotNull String getId() {
        return "clan_homes";
    }
}
