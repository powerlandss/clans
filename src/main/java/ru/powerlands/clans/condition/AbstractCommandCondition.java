package ru.powerlands.clans.condition;

import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.CommandConditions;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;

public abstract class AbstractCommandCondition extends AbstractCondition implements CommandConditions.Condition<BukkitCommandIssuer> {
    public AbstractCommandCondition(@NotNull ClanPlugin plugin) {
        super(plugin);
    }
}