package ru.powerlands.clans.condition;

import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.interfaces.IdentifiableCondition;
import ru.powerlands.clans.manager.ClanManager;

public abstract class AbstractCondition implements IdentifiableCondition {

    protected final ClanPlugin plugin;
    protected final ClanManager clanManager;

    public AbstractCondition(@NotNull ClanPlugin plugin) {
        this.plugin = plugin;
        this.clanManager = plugin.getManagerService().getClanManager();
    }
}