package ru.powerlands.clans.condition;

import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.CommandConditions;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;

public abstract class AbstractParameterCondition<T> extends AbstractCondition implements CommandConditions.ParameterCondition<T,
        BukkitCommandExecutionContext, BukkitCommandIssuer> {
    public AbstractParameterCondition(@NotNull ClanPlugin plugin) {
        super(plugin);
    }
    public abstract Class<T> getType();
}