package ru.powerlands.clans.condition.model;

import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.ConditionContext;
import co.aikar.commands.InvalidCommandArgument;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.condition.AbstractCommandCondition;
@SuppressWarnings("unused")
public class SenderClanMemberCondition extends AbstractCommandCondition {

    public SenderClanMemberCondition(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void validateCondition(ConditionContext<BukkitCommandIssuer> context) throws InvalidCommandArgument {
        clanManager.assertClanMember(context.getIssuer().getIssuer());
    }

    @Override
    public @NotNull String getId() {
        return "clan_member";
    }
}