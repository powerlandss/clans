package ru.powerlands.clans.condition.model;

import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.ConditionContext;
import co.aikar.commands.InvalidCommandArgument;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.condition.AbstractCommandCondition;

@SuppressWarnings("unused")
public class SenderClanOwnerCondition extends AbstractCommandCondition {

    public SenderClanOwnerCondition(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void validateCondition(ConditionContext<BukkitCommandIssuer> context) throws InvalidCommandArgument {
        clanManager.assertClanOwner(context.getIssuer().getIssuer());
    }

    @Override
    public @NotNull String getId() {
        return "clan_owner";
    }
}