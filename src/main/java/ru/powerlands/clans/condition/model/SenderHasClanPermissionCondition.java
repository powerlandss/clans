package ru.powerlands.clans.condition.model;

import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.ConditionContext;
import co.aikar.commands.InvalidCommandArgument;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.condition.AbstractCommandCondition;
import ru.powerlands.clans.model.Permission;

public class SenderHasClanPermissionCondition extends AbstractCommandCondition {

    public SenderHasClanPermissionCondition(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void validateCondition(ConditionContext<BukkitCommandIssuer> context) throws InvalidCommandArgument {
        String permission = context.getConfigValue("name", (String) null);
        clanManager.assertClanPermission(context.getIssuer().getIssuer(), Permission.valueOf(permission));
    }

    @Override
    public @NotNull String getId() {
        return "clan_permission";
    }
}