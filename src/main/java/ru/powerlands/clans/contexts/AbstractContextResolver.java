package ru.powerlands.clans.contexts;

import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.manager.ClanManager;

public abstract class AbstractContextResolver<T> {

    protected final @NotNull ClanPlugin plugin;
    protected final @NotNull ClanManager clanManager;

    public AbstractContextResolver(@NotNull ClanPlugin plugin) {
        this.plugin = plugin;
        clanManager = plugin.getManagerService().getClanManager();
    }

    public abstract Class<T> getType();
}