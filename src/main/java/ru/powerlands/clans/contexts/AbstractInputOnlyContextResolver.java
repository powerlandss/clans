package ru.powerlands.clans.contexts;

import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.contexts.ContextResolver;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;

public abstract class AbstractInputOnlyContextResolver<T> extends AbstractContextResolver<T>
        implements ContextResolver<T, BukkitCommandExecutionContext> {
    public AbstractInputOnlyContextResolver(@NotNull ClanPlugin plugin) {
        super(plugin);
    }
}