package ru.powerlands.clans.contexts;

import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.contexts.IssuerOnlyContextResolver;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;

public abstract class AbstractIssuerOnlyContextResolver<T> extends AbstractContextResolver<T>
        implements IssuerOnlyContextResolver<T, BukkitCommandExecutionContext> {
    public AbstractIssuerOnlyContextResolver(@NotNull ClanPlugin plugin) {
        super(plugin);
    }
}