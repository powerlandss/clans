package ru.powerlands.clans.contexts.model;

import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.InvalidCommandArgument;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.contexts.AbstractIssuerOnlyContextResolver;
import ru.powerlands.clans.model.Clan;

@SuppressWarnings("unused")
public class ClanContextResolver extends AbstractIssuerOnlyContextResolver<Clan> {
    public ClanContextResolver(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public Clan getContext(BukkitCommandExecutionContext context) throws InvalidCommandArgument {
        return clanManager.getClanByPlayer(context.getPlayer().getName());
    }

    @Override
    public Class<Clan> getType() {
        return Clan.class;
    }
}