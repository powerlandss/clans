package ru.powerlands.clans.contexts.model;

import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.InvalidCommandArgument;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.contexts.AbstractIssuerOnlyContextResolver;
import ru.powerlands.clans.model.ClanPlayer;

@SuppressWarnings("unused")
public class ClanPlayerContextResolver extends AbstractIssuerOnlyContextResolver<ClanPlayer> {
    public ClanPlayerContextResolver(@NotNull ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public ClanPlayer getContext(BukkitCommandExecutionContext context) throws InvalidCommandArgument {
        return clanManager.getClanPlayer(context.getPlayer().getName());
    }

    @Override
    public Class<ClanPlayer> getType() {
        return ClanPlayer.class;
    }
}