package ru.powerlands.clans.interfaces;

import org.jetbrains.annotations.NotNull;

public interface IdentifiableCompletion {

    @NotNull
    String getId();
}
