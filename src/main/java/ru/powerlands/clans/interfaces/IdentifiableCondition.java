package ru.powerlands.clans.interfaces;

import org.jetbrains.annotations.NotNull;

public interface IdentifiableCondition {
    @NotNull
    String getId();
}