package ru.powerlands.clans.interfaces;

public interface Manager {

    public default void load() {}
    public default void unload() {}
}
