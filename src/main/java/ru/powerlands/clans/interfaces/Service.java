package ru.powerlands.clans.interfaces;

public interface Service {

    public default void load() {}
    public default void unload() {}
}
