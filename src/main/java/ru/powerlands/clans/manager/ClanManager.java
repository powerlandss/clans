package ru.powerlands.clans.manager;

import co.aikar.commands.ConditionFailedException;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;
import ru.powerlands.clans.model.*;
import ru.powerlands.clans.service.BaseService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class ClanManager extends Instanced implements Manager {
    public ClanManager(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {

    }

    @Override
    public void unload() {

    }

    public ClanPlayer getOwner(Clan clan) {
        List<ClanPlayer> clanPlayerList = getClanPlayersByClanTag(clan.getTag()).stream().filter(clanPlayer -> clanPlayer.getStatus() == Status.OWNER).collect(Collectors.toList());
        if(clanPlayerList.isEmpty()) return null;
        return clanPlayerList.get(0);
    }

    public Clan getClanByPlayer(String player) {
        return getClanByTag(getClanPlayer(player).getClanTag());
    }
    public Clan getClanByTag(String tag) {
        final BaseService baseService = getPlugin().getBaseService();
        return baseService.getDatabaseManager().supplyAsync(() -> baseService.getClanDao().queryForId(tag)).join();
    }

    public ClanPlayer getClanPlayerByClanTag(String player, String tag) {
        final BaseService baseService = getPlugin().getBaseService();
        return baseService.getDatabaseManager().supplyAsync(() -> {
            QueryBuilder<ClanPlayer, String> queryBuilder = baseService.getClanPlayersDao().queryBuilder();
            Where<ClanPlayer, String> where = queryBuilder.where();
            where.eq("player", player);
            where.eq("clanTag", tag);

            List<ClanPlayer> clanPlayerList = queryBuilder.query();
            return clanPlayerList.isEmpty() ? null : clanPlayerList.get(0);
        }).join();
    }
    public List<ClanPlayer> getClanPlayersByClanTag(String tag) {
        final BaseService baseService = getPlugin().getBaseService();
        return baseService.getDatabaseManager().supplyAsync(() -> {
            QueryBuilder<ClanPlayer, String> queryBuilder = baseService.getClanPlayersDao().queryBuilder();
            Where<ClanPlayer, String> where = queryBuilder.where();
            where.eq("clanTag", tag);

            List<ClanPlayer> clanPlayerList = queryBuilder.query();
            return clanPlayerList.isEmpty() ? null : clanPlayerList;
        }).join();
    }
    public List<Clan> getClanByDisplay(String display) {
        final BaseService baseService = getPlugin().getBaseService();
        return baseService.getDatabaseManager().supplyAsync(() -> {
            QueryBuilder<Clan, String> queryBuilder = baseService.getClanDao().queryBuilder();
            Where<Clan, String> where = queryBuilder.where();
            where.eq("displayName", display);

            List<Clan> clanPlayerList = queryBuilder.query();
            return clanPlayerList.isEmpty() ? null : clanPlayerList;
        }).join();
    }

    public ClanPlayer getClanPlayer(String player) {
        final BaseService baseService = getPlugin().getBaseService();
        return baseService.getDatabaseManager().supplyAsync(() -> baseService.getClanPlayersDao().queryForId(player)).join();
    }
    public void assertClanPermission(CommandSender commandSender, Permission permission) throws ConditionFailedException {
        assertClanMember(commandSender);
        ClanPlayer clanPlayer = getClanPlayer(commandSender.getName());

        if (!checkClanPermission(clanPlayer, permission)) throw new ConditionFailedException(getPlugin().getManagerService().getConfigManager().getMessages().getString("messages.noPermission"));
    }
    public boolean checkClanPermission(ClanPlayer clanPlayer, Permission permission) {
        return clanPlayer.getPermissionList().contains(Permission.ALL) || clanPlayer.getPermissionList().contains(permission) || clanPlayer.getStatus() == Status.OWNER;
    }
    public void assertClanOwner(CommandSender commandSender) throws ConditionFailedException {
        ClanPlayer clanPlayer = getClanPlayer(commandSender.getName());
        if(clanPlayer == null || (clanPlayer.getStatus() != Status.OWNER && !clanPlayer.getPermissionList().contains(Permission.ALL))) throw new ConditionFailedException(getPlugin().getManagerService().getConfigManager().getMessages().getString("messages.notOwner"));

    }
    public void assertClanMember(CommandSender commandSender) throws ConditionFailedException {
        ClanPlayer clanPlayer = getClanPlayer(commandSender.getName());
        if(clanPlayer == null) throw new ConditionFailedException(getPlugin().getManagerService().getConfigManager().getMessages().getString("messages.notMember"));
    }
    public void assertNotClanMember(CommandSender commandSender) throws ConditionFailedException {
        ClanPlayer clanPlayer = getClanPlayer(commandSender.getName());
        if(clanPlayer != null) throw new ConditionFailedException(getPlugin().getManagerService().getConfigManager().getMessages().getString("messages.isMember"));
    }

    public Clan createClan(Player player, String name) {
        final BaseService baseService = getPlugin().getBaseService();
        Clan clan = new Clan(name, name, new ArrayList<>(), new ArrayList<>(), true, new ArrayList<>(), new Storage(name), GlowingColor.getSingleton());
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanDao().create(clan));
        createClanPlayer(player, clan, Status.OWNER);
        return clan;
    }
    public ClanPlayer createClanPlayer(Player player, Clan clan, Status status) {
        final BaseService baseService = getPlugin().getBaseService();
        String strStatus = "Участник";
        if(status == Status.OWNER) strStatus = "Владелец";
        ClanPlayer clanPlayer = new ClanPlayer(player.getName(), clan.getTag(), status, new ArrayList<>(), strStatus);
        clan.getPlayers().add(player.getName());
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanPlayersDao().create(clanPlayer));
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanDao().update(clan));
        return clanPlayer;
    }

    public void removeClan(Clan clan) {
        final BaseService baseService = getPlugin().getBaseService();
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanDao().delete(clan));

        List<ClanPlayer> clanPlayerList = getClanPlayersByClanTag(clan.getTag());
        clanPlayerList.forEach(clanPlayer -> removePlayer(clanPlayer, clan));
    }
    public void removePlayer(ClanPlayer clanPlayer, Clan clan) {
        final BaseService baseService = getPlugin().getBaseService();
        clan.getPlayers().remove(clanPlayer.getPlayer());
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanPlayersDao().delete(clanPlayer));
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanDao().update(clan));
    }
    public void updateClan(Clan clan) {
        final BaseService baseService = getPlugin().getBaseService();
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanDao().update(clan));
    }
    public void updateClanPlayer(ClanPlayer clanPlayer) {
        final BaseService baseService = getPlugin().getBaseService();
        baseService.getDatabaseManager().runAsync(()-> baseService.getClanPlayersDao().update(clanPlayer));
    }
}
