package ru.powerlands.clans.manager;

import co.aikar.commands.*;
import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.completion.AbstractAsyncCompletion;
import ru.powerlands.clans.completion.AbstractCompletion;
import ru.powerlands.clans.completion.AbstractStaticCompletion;
import ru.powerlands.clans.completion.AbstractSyncCompletion;
import ru.powerlands.clans.condition.AbstractCommandCondition;
import ru.powerlands.clans.condition.AbstractCondition;
import ru.powerlands.clans.condition.AbstractParameterCondition;
import ru.powerlands.clans.contexts.AbstractContextResolver;
import ru.powerlands.clans.contexts.AbstractInputOnlyContextResolver;
import ru.powerlands.clans.contexts.AbstractIssuerOnlyContextResolver;
import ru.powerlands.clans.interfaces.Manager;
import ru.powerlands.clans.util.MultiUtil;

import javax.annotation.Nullable;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

public class CommandManager extends BukkitCommandManager implements Manager {

    private final ClanPlugin clanPlugin;
    public CommandManager(ClanPlugin plugin) {
        super(plugin);
        this.clanPlugin = plugin;
    }

    @Override
    public void load() {
        registerContexts();
        registerDependencies();
        registerCompletions();
        registerConditions();
        registerCommands();
    }

    @Override
    public void unload() {
    }
    @SuppressWarnings("unchecked")
    private void registerDependencies() {
        registerDependency(ClanManager.class, getClanPlugin().getManagerService().getClanManager());
        registerDependency(ConfigManager.class, getClanPlugin().getManagerService().getConfigManager());
        registerDependency(MessageManager.class, getClanPlugin().getManagerService().getMessageManager());
        registerDependency(InviteManager.class, getClanPlugin().getManagerService().getInviteManager());
        registerDependency(LeaderBordManager.class, getClanPlugin().getManagerService().getLeaderBordManager());
        registerDependency(MenuManager.class, getClanPlugin().getManagerService().getMenuManager());
    }

    @SuppressWarnings("unchecked")
    private void registerCompletions() {
        Set<Class<? extends AbstractCompletion>> completions =
                MultiUtil.getSubTypesOf("ru.powerlands.clans.completion.model",
                        AbstractCompletion.class);
        plugin.getLogger().info(String.format("Registering %d command completions...", completions.size()));
        for (Class<? extends AbstractCompletion> c : completions) {
            if (Modifier.isAbstract(c.getModifiers())) {
                continue;
            }
            try {
                AbstractCompletion obj = c.getConstructor(ClanPlugin.class).newInstance(plugin);
                if (obj instanceof AbstractStaticCompletion) {
                    getCommandCompletions().registerStaticCompletion(obj.getId(),
                            ((AbstractStaticCompletion) obj).getCompletions());
                }
                if (obj instanceof AbstractAsyncCompletion) {
                    getCommandCompletions().registerAsyncCompletion(obj.getId(), (AbstractAsyncCompletion) obj);
                }
                if (obj instanceof AbstractSyncCompletion) {
                    getCommandCompletions().registerCompletion(obj.getId(), ((AbstractSyncCompletion) obj));
                }
            } catch (Exception ex) {
                plugin.getLogger().log(Level.SEVERE, "Error registering completion", ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void registerConditions() {
        Set<Class<? extends AbstractCondition>> conditions =
                MultiUtil.getSubTypesOf("ru.powerlands.clans.condition.model",
                        AbstractCondition.class);
        plugin.getLogger().info(String.format("Registering %d command conditions...", conditions.size()));
        for (Class<? extends AbstractCondition> c : conditions) {
            if (Modifier.isAbstract(c.getModifiers())) {
                continue;
            }
            try {
                AbstractCondition obj = c.getConstructor(ClanPlugin.class).newInstance(plugin);
                if (obj instanceof AbstractParameterCondition) {
                    @SuppressWarnings("rawtypes")
                    AbstractParameterCondition condition = (AbstractParameterCondition) obj;
                    getCommandConditions().addCondition(condition.getType(), condition.getId(), condition);
                }
                if (obj instanceof AbstractCommandCondition) {
                    AbstractCommandCondition condition = (AbstractCommandCondition) obj;
                    getCommandConditions().addCondition(condition.getId(), condition);
                }
            } catch (Exception ex) {
                plugin.getLogger().log(Level.SEVERE, "Error registering condition", ex);
            }
        }
    }
    @SuppressWarnings("unchecked")
    private void registerCommands() {
        boolean forceCommandPriority = true;
        Set<Class<? extends BaseCommand>> commands = MultiUtil.getSubTypesOf("ru.powerlands.clans.command", BaseCommand.class);
        plugin.getLogger().info(String.format("Registering %d base commands...", commands.size()));
        for (Class<? extends BaseCommand> c : commands) {
            if (c.isMemberClass() || Modifier.isStatic(c.getModifiers())) {
                continue;
            }
            try {
                BaseCommand baseCommand = c.getConstructor().newInstance();
                registerCommand(baseCommand, forceCommandPriority);
            } catch (Exception ex) {
                plugin.getLogger().log(Level.SEVERE, "Error registering command", ex);
            }
        }
    }
    @SuppressWarnings({"rawtypes", "unchecked"})
    private void registerContexts() {
        Set<Class<? extends AbstractContextResolver>> resolvers =
                MultiUtil.getSubTypesOf("ru.powerlands.clans.contexts.model",
                        AbstractContextResolver.class);
        plugin.getLogger().info(String.format("Registering %d command contexts...", resolvers.size()));
        for (Class<? extends AbstractContextResolver> cr : resolvers) {
            if (Modifier.isAbstract(cr.getModifiers())) {
                continue;
            }
            try {
                AbstractContextResolver obj = cr.getConstructor(ClanPlugin.class).newInstance(plugin);
                if (obj instanceof AbstractIssuerOnlyContextResolver) {
                    getCommandContexts().registerIssuerOnlyContext(obj.getType(), ((AbstractIssuerOnlyContextResolver) obj));
                }
                if (obj instanceof AbstractInputOnlyContextResolver) {
                    getCommandContexts().registerContext(obj.getType(), ((AbstractInputOnlyContextResolver<?>) obj));
                }
            } catch (Exception ex) {
                plugin.getLogger().log(Level.SEVERE, "Error registering context", ex);
            }
        }
    }

    @Override
    public BukkitLocales getLocales() {
        if (locales == null) {
            locales = new BukkitLocales(this) {

                @Override
                @Nullable
                public String getOptionalMessage(CommandIssuer issuer, MessageKey key) {
                    String rawKey = key.getKey();
                    rawKey = rawKey.replace("acf-minecraft", "language").replace("acf-core", "language");
                    return getClanPlugin().getManagerService().getConfigManager().getLanguage().getString(rawKey, rawKey);
                }

                @Override
                @NotNull
                public String getMessage(CommandIssuer issuer, MessageKeyProvider key) {
                    String rawKey = key.getMessageKey().getKey();
                    rawKey = rawKey.replace("acf-minecraft", "language").replace("acf-core", "language");
                    return getClanPlugin().getManagerService().getConfigManager().getLanguage().getString(rawKey, rawKey);
                }
            };
        }
        return this.locales;
    }





    public ClanPlugin getClanPlugin() {
        return clanPlugin;
    }
}
