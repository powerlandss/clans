package ru.powerlands.clans.manager;

import ru.landsproject.api.configuration.Configuration;
import ru.landsproject.api.configuration.Type;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;

public class ConfigManager extends Instanced implements Manager {

    private Configuration configuration;
    private Configuration language;
    private Configuration messages;
    private Configuration settings;
    private Configuration menus;
    private Configuration items;
    private Configuration rewards;
    public ConfigManager(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {
        configuration = new Configuration("config.yml", getPlugin().getDataFolder(), Type.YAML);
        language = new Configuration("language.yml", getPlugin().getDataFolder(), Type.YAML);
        messages = new Configuration("messages.yml", getPlugin().getDataFolder(), Type.YAML);
        settings = new Configuration("settings.yml", getPlugin().getDataFolder(), Type.YAML);
        menus = new Configuration("menu.yml", getPlugin().getDataFolder(), Type.YAML);
        items = new Configuration("items.yml", getPlugin().getDataFolder(), Type.YAML);
        rewards = new Configuration("rewards.yml", getPlugin().getDataFolder(), Type.YAML);

        handleConfiguration(configuration);
        handleConfiguration(language);
        handleConfiguration(messages);
        handleConfiguration(settings);
        handleConfiguration(menus);
        handleConfiguration(items);
        handleConfiguration(rewards);
    }

    @Override
    public void unload() {
        configuration.destruct();
        language.destruct();
    }

    private void handleConfiguration(Configuration configuration) {
        configuration.setaClass(getClass());
        configuration.useDefaultColorful();
        configuration.init();
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public Configuration getLanguage() {
        return language;
    }

    public Configuration getMessages() {
        return messages;
    }

    public Configuration getSettings() {
        return settings;
    }

    public Configuration getMenus() {
        return menus;
    }

    public Configuration getItems() {
        return items;
    }

    public Configuration getRewards() {
        return rewards;
    }
}
