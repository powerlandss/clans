package ru.powerlands.clans.manager;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.entity.Player;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;
import ru.powerlands.clans.model.Clan;

import java.util.concurrent.TimeUnit;

public class InviteManager extends Instanced implements Manager {

    private final Cache<Player, Clan> waitAccept = CacheBuilder.newBuilder().expireAfterWrite(30, TimeUnit.SECONDS).build();

    public InviteManager(ClanPlugin plugin) {
        super(plugin);
    }


    public Cache<Player, Clan> getWaitAccept() {
        return waitAccept;
    }
}
