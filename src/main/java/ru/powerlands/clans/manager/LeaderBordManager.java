package ru.powerlands.clans.manager;

import com.j256.ormlite.stmt.QueryBuilder;
import org.bukkit.Bukkit;
import ru.landsproject.api.database.DatabaseManager;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.service.BaseService;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LeaderBordManager extends Instanced implements Manager {

    private final ConcurrentHashMap<Integer, Clan> topBalance = new ConcurrentHashMap<>();
    private int seconds = 30;
    private int alertID;
    public LeaderBordManager(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {
        alertID = Bukkit.getScheduler().runTaskTimerAsynchronously(getPlugin(), ()-> {
            if (seconds <= 1) {
                seconds = 30;
                updateTopBalance();
            } else {
                seconds--;
            }
        }, 20, 20).getTaskId();
    }

    @Override
    public void unload() {
        Bukkit.getScheduler().cancelTask(alertID);
    }

    public void updateTopBalance() {
        final BaseService baseService = getPlugin().getBaseService();
        QueryBuilder<Clan, String> queryBuilder = baseService.getDatabaseManager().supplyAsync(()->baseService.getClanDao().queryBuilder()).join();
        queryBuilder.orderBy("rating", false);
        List<Clan> sortedClans = null;
        try {
            sortedClans = queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        topBalance.clear();
        for (int i = 0; i < sortedClans.size(); i++) {
            Clan clan = sortedClans.get(i);
            topBalance.put(i, clan);
        }
    }

    public int getClanPosition(Clan clan) {
        for (ConcurrentMap.Entry<Integer, Clan> entry : topBalance.entrySet()) {
            if (entry.getValue().getDisplayName().equals(clan.getDisplayName())) {
                return entry.getKey() + 1;
            }
        }
        return topBalance.size();
    }

    public ConcurrentHashMap<Integer, Clan> getTopBalance() {
        return topBalance;
    }

    public int getAlertID() {
        return alertID;
    }

    public int getSeconds() {
        return seconds;
    }
}
