package ru.powerlands.clans.manager;

import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;
import ru.powerlands.clans.menu.main.MainMenu;
import ru.powerlands.clans.menu.member.MemberMenu;
import ru.powerlands.clans.menu.reward.RewardMenu;

public class MenuManager extends Instanced implements Manager {

    private MainMenu mainMenu;
    private MemberMenu memberMenu;
    private RewardMenu rewardMenu;
    public MenuManager(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {
        mainMenu = new MainMenu(getPlugin());
        memberMenu = new MemberMenu(getPlugin());
        rewardMenu = new RewardMenu(getPlugin());
    }


    public MainMenu getMainMenu() {
        return mainMenu;
    }

    public MemberMenu getMemberMenu() {
        return memberMenu;
    }

    public RewardMenu getRewardMenu() {
        return rewardMenu;
    }
}
