package ru.powerlands.clans.manager;

import org.bukkit.entity.Player;
import ru.landsproject.api.configuration.Configuration;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;

public class MessageManager extends Instanced implements Manager {

    public MessageManager(ClanPlugin plugin) {
        super(plugin);
    }

    public void sendMessage(Player player, String message) {
        player.sendMessage(message);
    }
    public void sendMessageWithConfig(Player player, String path, Configuration conf) {
        player.sendMessage(conf.getString(path));
    }
    public void sendMessageWithPrefix(Player player, String message, Configuration configuration) {
        player.sendMessage(configuration.getString("messages.prefix") + message);
    }
    public void sendMessageWithPrefixAndConf(Player player, String path, Configuration configuration, String... replacing) {
        player.sendMessage(configuration.getString("messages.prefix") + replace(configuration.getString(path), replacing));
    }

    private String replace(String message, String... keyValues) {
        if (keyValues.length % 2 != 0) {
            throw new IllegalArgumentException("Количество элементов в массиве должно быть четным.");
        }
        for (int i = 0; i < keyValues.length; i += 2) {
            String key = keyValues[i];
            String value = keyValues[i + 1];
            message = message.replace(key, value);
        }
        return message;
    }
}
