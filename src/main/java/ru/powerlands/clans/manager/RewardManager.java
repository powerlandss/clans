package ru.powerlands.clans.manager;

import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class RewardManager extends Instanced implements Manager {

    public RewardManager(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {
        List<String> categoryKeys = new ArrayList<>(getPlugin().getManagerService().getConfigManager().getRewards().getConfigurationSection("items").getKeys(false));

        Comparator<String> categoryComparator = Comparator.comparingInt(key ->
                getPlugin().getManagerService().getConfigManager().getRewards().getInt("items." + key + ".level"));
        categoryKeys.sort(categoryComparator);
        rewards = categoryKeys;
    }

    @Override
    public void unload() {

    }
    private List<String> rewards = new ArrayList<>();

    public List<String> getRewards() {
        return rewards;
    }
}
