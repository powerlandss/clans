package ru.powerlands.clans.manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.landsproject.api.configuration.ItemManager;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.menu.storage.StorageMenu;
import ru.powerlands.clans.model.Clan;

import java.util.HashMap;

public class StorageManager extends Instanced implements Listener {
    public static final HashMap<Player, StorageMenu> STORAGE_HASH_MAP = new HashMap<>();
    public static final HashMap<String, Inventory> INVENTORY_HASH_MAP = new HashMap<>();

    public StorageManager(ClanPlugin plugin) {
        super(plugin);
    }


    @EventHandler
    private void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        
        if (player.hasMetadata("storageEditor")) {
            player.removeMetadata("storageEditor", getPlugin());
            StorageMenu storage = STORAGE_HASH_MAP.get(player);
            Clan clan = storage.getClan();
            for (int i = 0; i < storage.getInventory().getSize(); i++) {
                ItemStack itemStack = storage.getInventory().getItem(i);
                if (itemStack == null) {
                    clan.getStorage().getItemStackHashMap().remove(i);
                    continue;
                }
                if (!ItemManager.hasTag(getPlugin(), itemStack, "customItem")) {
                    clan.getStorage().getItemStackHashMap().put(i, storage.getInventory().getItem(i));
                }
            }
            getPlugin().getManagerService().getClanManager().updateClan(clan);
            STORAGE_HASH_MAP.remove(player);
        }
    }
    @EventHandler
    private void onClose(InventoryCloseEvent e) {
        Player player = (Player) e.getPlayer();
        if (player.hasMetadata("storageEditor")) {
            player.removeMetadata("storageEditor", getPlugin());
            StorageMenu storage = STORAGE_HASH_MAP.get(player);
            Clan clan = storage.getClan();
            for (int i = 0; i < storage.getInventory().getSize(); i++) {
                ItemStack itemStack = storage.getInventory().getItem(i);
                if (itemStack == null) {
                    clan.getStorage().getItemStackHashMap().remove(i);
                    continue;
                }
                if (!ItemManager.hasTag(getPlugin(), itemStack, "customItem")) {
                    clan.getStorage().getItemStackHashMap().put(i, storage.getInventory().getItem(i));
                }
            }
            getPlugin().getManagerService().getClanManager().updateClan(clan);
            STORAGE_HASH_MAP.remove(player);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player))
            return;
        Player player = (Player) e.getWhoClicked();
        if (!player.hasMetadata("storageEditor"))
            return;
        if (e.getCurrentItem() == null)
            return;
        ItemStack itemStack = e.getCurrentItem();
        String tag = ItemManager.getTag(getPlugin(), itemStack, "customItem");
        if (tag != null) {
            e.setCancelled(true);
            if (e.getSlot() == 4) {
                StorageMenu storage = STORAGE_HASH_MAP.get(player);
                if(storage == null) return;
                getPlugin().getManagerService().getClanManager().updateClan(storage.getClan());
                getPlugin().getManagerService().getMenuManager().getMainMenu().MAIN_MENU_INVENTORY().open(player);
            }
        }


    }
}
