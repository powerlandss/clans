package ru.powerlands.clans.menu.main;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.landsproject.api.configuration.ItemManager;
import ru.landsproject.api.configuration.Messager;
import ru.landsproject.api.inventoryapi.bukkit.BasicInventory;
import ru.landsproject.api.inventoryapi.bukkit.ClickableItem;
import ru.landsproject.api.inventoryapi.bukkit.content.InventoryContents;
import ru.landsproject.api.inventoryapi.bukkit.content.InventoryProvider;
import ru.landsproject.api.inventoryapi.bukkit.item.ItemBuilder;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.TriConsumer;
import ru.powerlands.clans.menu.storage.StorageMenu;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.service.ManagerService;
import ru.powerlands.clans.util.Constants;
import ru.powerlands.clans.util.MultiUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MainMenu extends Instanced implements InventoryProvider {


    public MainMenu(ClanPlugin plugin) {
        super(plugin);
    }

    public BasicInventory MAIN_MENU_INVENTORY() {
        return BasicInventory.builder().id("main_menu").provider(getPlugin().getManagerService().getMenuManager().getMainMenu()).size(5, 9).manager(getPlugin().getManagerService().getInventoryManager()).title(getPlugin().getManagerService().getConfigManager().getMenus().getString("main.title")).build();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        final ManagerService managerService = getPlugin().getManagerService();
        ItemStack commonItem = new ItemBuilder(managerService.getConfigManager().getItems(), "items.panel").build();
        Clan clan = managerService.getClanManager().getClanByPlayer(player.getName());
        for (int i : Constants.MAIN_MENU_PANELS) {
            contents.set(i, ClickableItem.empty(commonItem));
        }
        TriConsumer<Integer, String, Runnable> setItem = (slot, itemKey, runnable) -> {
            ItemStack itemStack = new ItemBuilder(managerService.getConfigManager().getItems(), itemKey).build();
            ItemMeta meta = itemStack.getItemMeta();

            if (meta.hasLore()) {
                meta.setLore(meta.getLore().stream().map(str -> replace(player, clan, str)).collect(Collectors.toList()));
            }

            itemStack.setItemMeta(meta);
            contents.set(slot, ClickableItem.of(itemStack, (event)-> runnable.run()));
        };
        setItem.accept(34, "items.storage", ()-> new StorageMenu(getPlugin(), player, clan).open());
        setItem.accept(13, "items.leaderbord",()->{});
        // TODO: 30.10.2023 realize glow menu 
        setItem.accept(14, "items.glow", ()-> {});
        setItem.accept(12, "items.level", ()-> managerService.getMenuManager().getRewardMenu().REWARD_MENU_INVENTORY().open(player));
        setItem.accept(11, "items.members", ()-> managerService.getMenuManager().getMemberMenu().MEMBER_MENU_INVENTORY().open(player));
        setItem.accept(10, "items.info", ()-> {});
        ItemStack colorItem = new ItemStack(Material.NAME_TAG);
        ItemMeta colorMeta = colorItem.getItemMeta();
        List<String> colorList = new ArrayList<>();
        colorMeta.setDisplayName(Messager.color("&6Доступные цветовые коды"));
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (String color : clan.getColors()) {
            if (i >= 3) {
                colorList.add(Messager.color(builder.toString()));
                builder = new StringBuilder();
                i = 0;
            }

            if (color.equalsIgnoreCase("rgb")) {
                color = "&#f90000R&#a60053G&#5300a6B";
                builder.append(" §7 - ").append(Messager.color(color)).append(" ");
                i++;
                continue;
            }

            builder.append(" §7 - ").append(Messager.color("&" + color) + "&").append(Messager.color("&" + color)).append(color).append(" ");
            i++;
        }
        colorList.add(Messager.color(builder.toString()));
        colorList.addAll(managerService.getConfigManager().getConfiguration().getStringList("messages.colors"));
        colorMeta.setLore(colorList);
        colorItem.setItemMeta(colorMeta);
        contents.set(28, ClickableItem.empty(colorItem));
    }

    private String replace(Player player, Clan clan, String str) {
        String state = "disable";
        
        if(!clan.getPvp()) {
            state = "enable";
        }

        str = str
                .replace("<name>", clan.getDisplayName())
                .replace("<xp>", MultiUtil.format(clan.getRating()) + "")
                .replace("<size>", clan.getPlayers().size() + "")
                .replace("<max>", clan.getLimitMembers() + "")
                .replace("<bank_balance>", MultiUtil.format(clan.getMoney()) + "")
                .replace("<bank_limit>", MultiUtil.format(clan.getMoneyLimit()) + "")
                .replace("<storage_limit>", clan.getLimitSlots() + "")
                .replace("<max_length>", clan.getLengthLimit() + "")
                .replace("<level>", clan.getLevel() + "")
                .replace("<pvp>", Objects.requireNonNull(getPlugin().getManagerService().getConfigManager().getMessages().getString("messages.pvp." + state)));
        return Messager.parseApi(str, player);
    }


    @Override
    public void update(Player player, InventoryContents contents) {

    }
}

