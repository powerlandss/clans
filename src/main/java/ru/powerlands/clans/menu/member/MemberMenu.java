package ru.powerlands.clans.menu.member;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import ru.landsproject.api.configuration.ItemManager;
import ru.landsproject.api.inventoryapi.bukkit.BasicInventory;
import ru.landsproject.api.inventoryapi.bukkit.ClickableItem;
import ru.landsproject.api.inventoryapi.bukkit.content.InventoryContents;
import ru.landsproject.api.inventoryapi.bukkit.content.InventoryProvider;
import ru.landsproject.api.inventoryapi.bukkit.content.Pagination;
import ru.landsproject.api.inventoryapi.bukkit.content.SlotIterator;
import ru.landsproject.api.inventoryapi.bukkit.item.ItemBuilder;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Permission;
import ru.powerlands.clans.service.ManagerService;
import ru.powerlands.clans.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class MemberMenu extends Instanced implements InventoryProvider {


    public MemberMenu(ClanPlugin plugin) {
        super(plugin);
    }

    public BasicInventory MEMBER_MENU_INVENTORY() {
        return BasicInventory.builder().id("member_menu").provider(getPlugin().getManagerService().getMenuManager().getMemberMenu()).size(5, 9).manager(getPlugin().getManagerService().getInventoryManager()).title(getPlugin().getManagerService().getConfigManager().getMenus().getString("member.title")).build();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        update(player, contents, 1);
    }


    @Override
    public void update(Player player, InventoryContents contents) {
    }
    public void update(Player player, InventoryContents contents, int rawSelected) {
        final ManagerService managerService = getPlugin().getManagerService();
        ClanManager clanManager = managerService.getClanManager();

        Clan clan = clanManager.getClanByPlayer(player.getName());
        ClanPlayer editClanPlayer = clanManager.getClanPlayer(player.getName());

        Pagination pagination = contents.pagination();
        AtomicInteger selected = new AtomicInteger(rawSelected);

        ItemStack commonItem = new ItemBuilder(managerService.getConfigManager().getItems(), "items.panel").build();
        ItemStack back = new ItemBuilder(managerService.getConfigManager().getItems(), "items.lastPage").build();
        ItemStack next = new ItemBuilder(managerService.getConfigManager().getItems(), "items.nextPage").build();

        ClickableItem[] clickableItems = new ClickableItem[clan.getPlayers().size()];

        contents.set(18, ClickableItem.of(back, e -> MEMBER_MENU_INVENTORY().open(player, pagination.previous().getPage())));
        contents.set(26, ClickableItem.of(next, e -> MEMBER_MENU_INVENTORY().open(player, pagination.next().getPage())));

        for (int i : Constants.MEMBER_MENU_PANELS) {
            contents.set(i, ClickableItem.empty(commonItem));
        }

        contents.set(4, ClickableItem.of(new ItemBuilder(managerService.getConfigManager().getItems(), "items.back").build(), e -> managerService.getMenuManager().getMainMenu().MAIN_MENU_INVENTORY().open(player)));

        int clickInner = 0;

        for (ClanPlayer clanPlayer : clanManager.getClanPlayersByClanTag(clan.getTag())) {
            ItemStack itemStack = new ItemBuilder(managerService.getConfigManager().getItems(), "items.player").build();
            itemStack = ItemManager.setTag(getPlugin(), itemStack, "memberPlayer", clanPlayer.getPlayer());

            ItemMeta itemMeta = itemStack.getItemMeta();

            if (itemMeta instanceof SkullMeta) {
                itemMeta = itemStack.getItemMeta();
                ((SkullMeta) itemMeta).setOwningPlayer(player);
            }

            itemMeta.setDisplayName(itemMeta.getDisplayName().replace("<prefix>", clanPlayer.getPrefix()).replace("<player>", clanPlayer.getPlayer()));
            List<String> list = new ArrayList<>();

            if (clanManager.getOwner(clan).getPlayer().equals(clanPlayer.getPlayer())) {
                list.add(managerService.getConfigManager().getItems().getString("items.player.owner"));
            } else {
                list.addAll(managerService.getConfigManager().getConfiguration().getStringList("messages.lore"));
                int inner = 1;

                for (Permission clanPermission : Permission.values()) {
                    if (inner == selected.get()) {
                        String permissionMessage = managerService.getConfigManager().getItems().getString("items.player.select") +
                                getPlugin().getManagerService().getConfigManager().getMessages().getString("messages." + clanPermission.name());
                        list.add(clanManager.checkClanPermission(clanPlayer, clanPermission) ? permissionMessage + "§a [Да]" : permissionMessage + "§c [Нет]");
                    } else {
                        String permissionMessage = managerService.getConfigManager().getItems().getString("items.player.noSelect") +
                                getPlugin().getManagerService().getConfigManager().getMessages().getString("messages." + clanPermission.name());
                        list.add(clanManager.checkClanPermission(clanPlayer, clanPermission) ? permissionMessage + "§a Да" : permissionMessage + "§c Нет");
                    }
                    inner++;
                }
            }

            itemMeta.setLore(list);
            itemStack.setItemMeta(itemMeta);
            clickableItems[clickInner] = ClickableItem.of(itemStack, e -> {
                switch (e.getClick()) {
                    case RIGHT: {
                        selected.addAndGet(1);
                        if (selected.get() > Permission.values().length) {
                            selected.set(1);
                        }
                        update(player, contents, selected.get());
                        break;
                    }
                    case LEFT: {
                        if (clanManager.getOwner(clan).getPlayer().equals(clanPlayer.getPlayer())) return;
                        if (clanManager.checkClanPermission(editClanPlayer, Permission.EDIT_PERMISSIONS)) {
                            Permission clanPermission = Permission.values()[selected.get() - 1];

                            if (clanManager.checkClanPermission(clanPlayer, clanPermission)) {
                                clanPlayer.getPermissionList().remove(clanPermission);
                            } else {
                                clanPlayer.getPermissionList().add(clanPermission);
                            }

                            getPlugin().getManagerService().getClanManager().updateClanPlayer(clanPlayer);
                            update(player, contents, selected.get());
                        }
                        break;
                    }
                }
            });
            clickInner++;
        }

        pagination.setItems(clickableItems);
        pagination.setItemsPerPage(21);
        SlotIterator slotIterator = contents.newIterator(SlotIterator.Type.HORIZONTAL, 1, 1);
        slotIterator.allowOverride(false);
        pagination.addToIterator(slotIterator);
    }
}

