package ru.powerlands.clans.menu.reward;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.landsproject.api.configuration.Messager;
import ru.landsproject.api.inventoryapi.bukkit.BasicInventory;
import ru.landsproject.api.inventoryapi.bukkit.ClickableItem;
import ru.landsproject.api.inventoryapi.bukkit.content.InventoryContents;
import ru.landsproject.api.inventoryapi.bukkit.content.InventoryProvider;
import ru.landsproject.api.inventoryapi.bukkit.content.Pagination;
import ru.landsproject.api.inventoryapi.bukkit.content.SlotIterator;
import ru.landsproject.api.inventoryapi.bukkit.item.ItemBuilder;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.service.ManagerService;
import ru.powerlands.clans.util.Constants;
import ru.powerlands.clans.util.MultiUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RewardMenu extends Instanced implements InventoryProvider {


    public RewardMenu(ClanPlugin plugin) {
        super(plugin);
    }

    public BasicInventory REWARD_MENU_INVENTORY() {
        return BasicInventory.builder().id("reward_menu").provider(getPlugin().getManagerService().getMenuManager().getRewardMenu()).size(6, 9).manager(getPlugin().getManagerService().getInventoryManager()).title(getPlugin().getManagerService().getConfigManager().getMenus().getString("reward.title")).build();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        final ManagerService managerService = getPlugin().getManagerService();
        ClanManager clanManager = managerService.getClanManager();
        Clan clan = clanManager.getClanByPlayer(player.getName());
//        ClanPlayer clanPlayer = clanManager.getClanPlayer(player.getName());
        Pagination pagination = contents.pagination();
        ItemStack commonItem = new ItemBuilder(managerService.getConfigManager().getItems(), "items.panel").build();
        ItemStack back = new ItemBuilder(managerService.getConfigManager().getItems(), "items.lastPage").build();
        ItemStack next = new ItemBuilder(managerService.getConfigManager().getItems(), "items.nextPage").build();

        ClickableItem[] clickableItems = new ClickableItem[managerService.getRewardManager().getRewards().size()];

        contents.set(18, ClickableItem.of(back, e -> REWARD_MENU_INVENTORY().open(player, pagination.previous().getPage())));
        contents.set(26, ClickableItem.of(next, e -> REWARD_MENU_INVENTORY().open(player, pagination.next().getPage())));

        for (int i : Constants.REWARD_MENU_PANELS) {
            contents.set(i, ClickableItem.empty(commonItem));
        }

        contents.set(2, ClickableItem.of(new ItemBuilder(managerService.getConfigManager().getItems(), "items.back").build(), (e)-> managerService.getMenuManager().getMainMenu().MAIN_MENU_INVENTORY().open(player)));
        {
            contents.set(4, ClickableItem.of(new ItemBuilder(managerService.getConfigManager().getItems(), "items.dop_info").build(), (e)-> managerService.getMenuManager().getMainMenu().MAIN_MENU_INVENTORY().open(player)));
            contents.set(5, ClickableItem.of(new ItemBuilder(managerService.getConfigManager().getItems(), "items.dop_info1").build(), (e)-> managerService.getMenuManager().getMainMenu().MAIN_MENU_INVENTORY().open(player)));
            {
                ItemStack leaderBord = new ItemBuilder(managerService.getConfigManager().getItems(), "items.leaderbord").build();
                ItemMeta meta = leaderBord.getItemMeta();

                meta.setLore(meta.getLore().stream().map(str -> replace(player, clan, str)).collect(Collectors.toList()));

                leaderBord.setItemMeta(meta);
                contents.set(6, ClickableItem.empty(leaderBord));
            }
        }
        contents.set(45, ClickableItem.of(back, e -> REWARD_MENU_INVENTORY().open(player, pagination.previous().getPage())));
        contents.set(53, ClickableItem.of(next, e -> REWARD_MENU_INVENTORY().open(player, pagination.next().getPage())));

        List<String> categoryKeys = getPlugin().getManagerService().getRewardManager().getRewards();

        int i = 1;
        int inner = 0;
        for (String str : categoryKeys) {
            ItemStack itemStack = new ItemBuilder(getPlugin().getManagerService().getConfigManager().getRewards(), "items." + str).build();
            if (i < clan.getLevel()) {
                itemStack = new ItemBuilder(getPlugin().getManagerService().getConfigManager().getRewards(), "item.success").build();
                ItemMeta meta = itemStack.getItemMeta();
                meta.setDisplayName(replace(meta.getDisplayName(), i, clan));
                itemStack.setItemMeta(meta);
                clickableItems[inner] = ClickableItem.empty(itemStack);
                i++;
                inner++;
                continue;
            }
            ItemMeta meta = itemStack.getItemMeta();
            i++;

            List<String> lore = meta.getLore();
            if (lore != null) {
                lore = lore.stream().map(s -> replace(s, getPlugin().getManagerService().getConfigManager().getRewards().getInt("items." + str + ".need"), clan))
                        .collect(Collectors.toList());

                meta.setLore(lore);
                itemStack.setItemMeta(meta);
            }
            clickableItems[inner] = ClickableItem.empty(itemStack);
            inner++;
        }

        pagination.setItems(clickableItems);
        pagination.setItemsPerPage(28);
        SlotIterator slotIterator = contents.newIterator(SlotIterator.Type.HORIZONTAL, 1, 1);
        slotIterator.allowOverride(false);
        pagination.addToIterator(slotIterator);
    }
    private String replace(Player player, Clan clan, String str) {
        String state = "disable";

        if(!clan.getPvp()) {
            state = "enable";
        }

        str = str
                .replace("<name>", clan.getDisplayName())
                .replace("<xp>", MultiUtil.format(clan.getRating()) + "")
                .replace("<size>", clan.getPlayers().size() + "")
                .replace("<max>", clan.getLimitMembers() + "")
                .replace("<bank_balance>", MultiUtil.format(clan.getMoney()) + "")
                .replace("<bank_limit>", MultiUtil.format(clan.getMoneyLimit()) + "")
                .replace("<storage_limit>", clan.getLimitSlots() + "")
                .replace("<max_length>", clan.getLengthLimit() + "")
                .replace("<level>", clan.getLevel() + "")
                .replace("<pvp>", Objects.requireNonNull(getPlugin().getManagerService().getConfigManager().getMessages().getString("messages.pvp." + state)));
        return Messager.parseApi(str, player);
    }
    private String replace(String str, Integer need, Clan clan) {
        return str.replace("<now>", clan.getRating() + "").replace("<need>", need + "").replace("<level>", need + "");
    }
}