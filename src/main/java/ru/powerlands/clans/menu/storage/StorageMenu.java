package ru.powerlands.clans.menu.storage;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import ru.landsproject.api.configuration.ItemManager;
import ru.landsproject.api.inventoryapi.bukkit.item.ItemBuilder;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.manager.StorageManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Permission;
import ru.powerlands.clans.model.Storage;
import ru.powerlands.clans.util.Constants;

public class StorageMenu extends Instanced {
    private Player player;
    private Clan clan;
    private Inventory inventory;
    private Storage storage;
    private ClanPlayer clanPlayer;

    public StorageMenu(ClanPlugin clanPlugin, Player player, Clan clan) {
        super(clanPlugin);
        this.player = player;
        this.clan = clan;
        setStorage(clan.getStorage());
        clanPlayer = clanPlugin.getManagerService().getClanManager().getClanPlayer(player.getName());
        setupMetadata();
    }

    public void fill() {
        fillInventoryPanels();
        fillEmptySlots();
        adjustLimitSlots();
        fillStorageItems();
        setBackButton();
    }

    public void open() {
        if (!hasStoragePermission()) return;
        initializeInventory();
        fill();
        player.openInventory(inventory);
        setupMetadata();
    }

    private void fillInventoryPanels() {
        ItemStack panelItem = createItem("items.panel");
        for (int i : Constants.STORAGE_MENU_PANELS) {
            inventory.setItem(i, panelItem);
        }
    }

    private void fillEmptySlots() {
        ItemStack dyeItem = createItem("items.dye");
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null) {
                inventory.setItem(i, dyeItem);
            }
        }
    }

    private void adjustLimitSlots() {
        int inner = 1;
        ItemStack panelItem = createItem("items.panel");
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack item = inventory.getItem(i);
            if (item == null || item.equals(panelItem)) continue;
            if (inner <= clan.getLimitSlots()) {
                inventory.setItem(i, new ItemStack(Material.AIR));
            }
            inner++;
        }
    }

    private void fillStorageItems() {
        int i = 1;
        for (int slot : storage.getItemStackHashMap().keySet()) {
            ItemStack item = storage.getItemStackHashMap().get(slot);
            inventory.setItem(slot, item);
            i++;
        }
    }

    private void setBackButton() {
        ItemStack backButton = createItem("items.back");
        inventory.setItem(4, ItemManager.setTag(getPlugin(), backButton, "customItem", "true"));
    }

    private boolean hasStoragePermission() {
        return getPlugin().getManagerService().getClanManager().checkClanPermission(clanPlayer, Permission.USE_STORAGE);
    }

    private void initializeInventory() {
        inventory = StorageManager.INVENTORY_HASH_MAP.computeIfAbsent(clan.getTag(), tag ->
                Bukkit.createInventory(null, 45, getPlugin().getManagerService().getConfigManager().getMenus().getString("storage.title")));
    }

    private void setupMetadata() {
        player.setMetadata("storageEditor", new FixedMetadataValue(getPlugin(), "storageEditor"));
        StorageManager.STORAGE_HASH_MAP.put(player, this);
    }
    private ItemStack createItem(String path) {
        return ItemManager.setTag(getPlugin(), new ItemBuilder(getPlugin().getManagerService().getConfigManager().getItems(), path).build(), "customItem", "true");
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Clan getClan() {
        return clan;
    }

    public void setClan(Clan clan) {
        this.clan = clan;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public ClanPlayer getClanPlayer() {
        return clanPlayer;
    }

    public void setClanPlayer(ClanPlayer clanPlayer) {
        this.clanPlayer = clanPlayer;
    }
}