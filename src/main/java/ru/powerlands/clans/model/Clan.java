package ru.powerlands.clans.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.jetbrains.annotations.NotNull;
import ru.powerlands.clans.util.persister.*;

import java.io.Serializable;
import java.util.List;

@DatabaseTable(tableName = "clans_main")
public class Clan implements Comparable<Clan>, Serializable {
    @DatabaseField(id = true)
    private String tag;
    @DatabaseField()
    private String displayName;

    // TODO: 29.10.2023 remove
    @DatabaseField(defaultValue = "5")
    private Long lengthLimit;
    @DatabaseField(defaultValue = "1")
    private Long level;
    @DatabaseField(persisterClass = ListStringPersister.class)
    private List<String> players;
    @DatabaseField(persisterClass = ListStringPersister.class)
    private List<String> colors;
    @DatabaseField(defaultValue = "3")
    private Long limitMembers;
    @DatabaseField(defaultValue = "2")
    private Long limitSlots;
    @DatabaseField(defaultValue = "1")
    private Long limitHomes;
    @DatabaseField()
    private Boolean pvp;
    @DatabaseField(persisterClass = HomePersister.class)
    private List<Home> homes;
    @DatabaseField(defaultValue = "0")
    private Long money;
    @DatabaseField(defaultValue = "100000")
    private Long moneyLimit;
    @DatabaseField(persisterClass = StoragePersister.class)
    private Storage storage;
    @DatabaseField(defaultValue = "0")
    private Long rating;
    @DatabaseField(defaultValue = "false")
    private Boolean glowing;
    @DatabaseField(persisterClass = GlowingColorPersister.class)
    private GlowingColor glowingColor = GlowingColor.getSingleton();

    public Clan(
            String tag,
            String displayName,
            List<String> players,
            List<String> colors,
            Boolean pvp,
            List<Home> homes,
            Storage storage,
            GlowingColor glowingColor
    ) {
        this.tag = tag;
        this.displayName = displayName;
        this.players = players;
        this.colors = colors;
        this.pvp = pvp;
        this.homes = homes;
        this.storage = storage;
        this.glowingColor = glowingColor;
    }

    public Clan() {
    }

    public String getTag() {
        return tag;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Long getLengthLimit() {
        return lengthLimit;
    }

    public void setLengthLimit(Long lengthLimit) {
        this.lengthLimit = lengthLimit;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public Long getLimitMembers() {
        return limitMembers;
    }

    public void setLimitMembers(Long limitMembers) {
        this.limitMembers = limitMembers;
    }

    public Long getLimitSlots() {
        return limitSlots;
    }

    public void setLimitSlots(Long limitSlots) {
        this.limitSlots = limitSlots;
    }

    public Long getLimitHomes() {
        return limitHomes;
    }

    public void setLimitHomes(Long limitHomes) {
        this.limitHomes = limitHomes;
    }

    public Boolean getPvp() {
        return pvp;
    }

    public void setPvp(Boolean pvp) {
        this.pvp = pvp;
    }

    public List<Home> getHomes() {
        return homes;
    }

    public void setHomes(List<Home> homes) {
        this.homes = homes;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public Long getMoneyLimit() {
        return moneyLimit;
    }

    public void setMoneyLimit(Long moneyLimit) {
        this.moneyLimit = moneyLimit;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public Boolean getGlowing() {
        return glowing;
    }

    public void setGlowing(Boolean glowing) {
        this.glowing = glowing;
    }

    @Override
    public int compareTo(@NotNull Clan other) {
        return this.getTag().compareToIgnoreCase(other.getTag());
    }
}
