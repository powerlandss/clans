package ru.powerlands.clans.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ru.powerlands.clans.util.persister.ListPermissionPersister;

import java.util.ArrayList;
import java.util.List;

@DatabaseTable(tableName = "clans_players")
public class ClanPlayer implements Comparable<ClanPlayer> {
    @DatabaseField(id = true)
    private String player;
    @DatabaseField()
    private String clanTag;
    @DatabaseField()
    private Status status;
    @DatabaseField(persisterClass = ListPermissionPersister.class)
    private List<Permission> permissionList = new ArrayList<>();
    @DatabaseField()
    private String prefix;

    public ClanPlayer(String player, String clanTag, Status status, List<Permission> permissionList, String prefix) {
        this.player = player;
        this.clanTag = clanTag;
        this.status = status;
        this.permissionList = permissionList;
        this.prefix = prefix;
    }

    public ClanPlayer() {
    }

    public String getPlayer() {
        return player;
    }

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getClanTag() {
        return clanTag;
    }

    public void setClanTag(String clanTag) {
        this.clanTag = clanTag;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public int compareTo(ClanPlayer other) {
        return getPlayer().compareTo(other.getPlayer());
    }
}
