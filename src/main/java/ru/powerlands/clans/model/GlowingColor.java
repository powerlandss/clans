package ru.powerlands.clans.model;

import java.io.Serializable;

public class GlowingColor implements Serializable {

    private static final GlowingColor singleton = new GlowingColor(0,255,130);

    public static GlowingColor getSingleton() {
        return singleton;
    }

    private int red;
    private int green;
    private int blue;

    public GlowingColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public GlowingColor() {
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }
}
