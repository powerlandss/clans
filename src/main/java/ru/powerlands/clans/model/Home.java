package ru.powerlands.clans.model;

import org.bukkit.Location;

import java.io.Serializable;

public class Home implements Serializable {

    private String name;
    private String world;
    private Location location;

    public Home(String name, String world, Location location) {
        this.name = name;
        this.world = world;
        this.location = location;
    }

    public Home() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
