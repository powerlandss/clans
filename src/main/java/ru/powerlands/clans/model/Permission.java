package ru.powerlands.clans.model;

public enum Permission {
    SHOW_MONEY,
    WITHDRAW_MONEY,
    INVEST_MONEY,
    INVITE_PLAYERS,
    KICK_PLAYERS,
    PVP_TOGGLE,
    CLAN_RENAME,
    CLAN_SETHOME,
    CLAN_DELHOME,
    USE_STORAGE,
    CLAN_GLOW,
    EDIT_PERMISSIONS,
    ALL
}
