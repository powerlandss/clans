package ru.powerlands.clans.model;

public enum Status {

    OWNER,
    MEMBER
}
