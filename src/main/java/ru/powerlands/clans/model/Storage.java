package ru.powerlands.clans.model;

import org.bukkit.inventory.ItemStack;

import java.io.Serializable;
import java.util.HashMap;

public class Storage implements Serializable {

    private String clanTag;
    private final HashMap<Integer, ItemStack> itemStackHashMap = new HashMap<>();

    public Storage(String clanTag) {
        this.clanTag = clanTag;
    }

    public Storage() {
    }

    public String getClanTag() {
        return clanTag;
    }

    public void setClanTag(String clanTag) {
        this.clanTag = clanTag;
    }

    public HashMap<Integer, ItemStack> getItemStackHashMap() {
        return itemStackHashMap;
    }
}
