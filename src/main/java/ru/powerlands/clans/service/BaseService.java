package ru.powerlands.clans.service;

import com.j256.ormlite.dao.Dao;
import ru.landsproject.api.configuration.Configuration;
import ru.landsproject.api.database.Database;
import ru.landsproject.api.database.DatabaseManager;
import ru.landsproject.api.exception.CredentialsParseException;
import ru.landsproject.api.exception.DriverLoadException;
import ru.landsproject.api.exception.DriverNotFoundException;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Service;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;

import java.sql.SQLException;

public class BaseService extends Instanced implements Service {
    private Database database;
    private DatabaseManager databaseManager;

    private Dao<Clan, String> clanDao;
    private Dao<ClanPlayer, String> clanPlayersDao;

    public BaseService(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {
        getPlugin().getLogger().info("Creating database thread pool");
        final Configuration configuration = getPlugin().getManagerService().getConfigManager().getConfiguration();
        try {
            database = new Database(getPlugin(), configuration);
            databaseManager = new DatabaseManager(getPlugin(), configuration, database);
        } catch (CredentialsParseException | DriverNotFoundException | DriverLoadException | SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            getPlugin().getLogger().info("Creating table and dao");
            databaseManager.createTable(Clan.class);
            databaseManager.createTable(ClanPlayer.class);
            clanDao = (Dao<Clan, String>) databaseManager.createDao(Clan.class);
            clanPlayersDao = (Dao<ClanPlayer, String>) databaseManager.createDao(ClanPlayer.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void unload() {
        getPlugin().getLogger().info("Close database thread pool");
        databaseManager.shutdown();
    }

    public Database getDatabase() {
        return database;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public Dao<Clan, String> getClanDao() {
        return clanDao;
    }

    public Dao<ClanPlayer, String> getClanPlayersDao() {
        return clanPlayersDao;
    }
}
