package ru.powerlands.clans.service;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Service;
import ru.powerlands.clans.manager.ClanManager;
import ru.powerlands.clans.manager.StorageManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.util.EventUtil;

public class ListenerService extends Instanced implements Service {

    public ListenerService(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {
        registerListeners();
    }

    @Override
    public void unload() {

    }
    public void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new StorageManager(getPlugin()), getPlugin());
        EventUtil.observeAll(EntityDamageByEntityEvent.class, (e -> {
            if (e.isCancelled() || e.getEntity().getType() != EntityType.PLAYER || e.getDamager().getType() != EntityType.PLAYER) {
                return;
            }
            final ClanManager clanManager = getPlugin().getManagerService().getClanManager();
            Player damager = (Player) e.getDamager();
            Player entity = (Player) e.getEntity();

            ClanPlayer clanPlayerDamager = clanManager.getClanPlayer(damager.getName());
            if(clanPlayerDamager == null) return;
            Clan clanDamager = clanManager.getClanByTag(clanPlayerDamager.getClanTag());
            if(clanDamager.getPlayers().contains(entity.getName())) {
                if(clanDamager.getPvp()) {
                    e.setCancelled(true);
                }
            }
        }));

    }
}
