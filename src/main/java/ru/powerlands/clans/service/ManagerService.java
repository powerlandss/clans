package ru.powerlands.clans.service;

import ru.landsproject.api.inventoryapi.bukkit.InventoryManager;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Service;
import ru.powerlands.clans.manager.*;

public class ManagerService extends Instanced implements Service {


    private CommandManager commandManager;
    private ConfigManager configManager;
    private ClanManager clanManager;
    private MessageManager messageManager;
    private InviteManager inviteManager;
    private LeaderBordManager leaderBordManager;
    private PlaceholderManager placeholderManager;
    private InventoryManager inventoryManager;
    private MenuManager menuManager;
    private RewardManager rewardManager;

    public ManagerService(ClanPlugin plugin) {
        super(plugin);
    }

    @Override
    public void load() {
        commandManager = new CommandManager(getPlugin());
        configManager = new ConfigManager(getPlugin());
        clanManager = new ClanManager(getPlugin());
        messageManager = new MessageManager(getPlugin());
        inviteManager = new InviteManager(getPlugin());
        leaderBordManager = new LeaderBordManager(getPlugin());
        placeholderManager = new PlaceholderManager(getPlugin());
        inviteManager = new InviteManager(getPlugin());
        inventoryManager = new InventoryManager(getPlugin());
        menuManager = new MenuManager(getPlugin());
        rewardManager = new RewardManager(getPlugin());

        commandManager.load();
        configManager.load();
        clanManager.load();
        messageManager.load();
        inviteManager.load();
        leaderBordManager.load();
        placeholderManager.load();
        inventoryManager.init();
        menuManager.load();
        rewardManager.load();
    }

    @Override
    public void unload() {
        commandManager.unload();
        configManager.unload();
        clanManager.unload();
        messageManager.unload();
        inviteManager.unload();
        leaderBordManager.unload();
        placeholderManager.unload();
        menuManager.unload();
        rewardManager.unload();
    }


    public CommandManager getCommandManager() {
        return commandManager;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public ClanManager getClanManager() {
        return clanManager;
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }

    public InviteManager getInviteManager() {
        return inviteManager;
    }

    public LeaderBordManager getLeaderBordManager() {
        return leaderBordManager;
    }

    public PlaceholderManager getPlaceholderManager() {
        return placeholderManager;
    }

    public InventoryManager getInventoryManager() {
        return inventoryManager;
    }

    public MenuManager getMenuManager() {
        return menuManager;
    }

    public RewardManager getRewardManager() {
        return rewardManager;
    }
}
