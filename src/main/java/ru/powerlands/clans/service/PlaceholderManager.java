package ru.powerlands.clans.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.landsproject.api.configuration.Messager;
import ru.powerlands.clans.ClanPlugin;
import ru.powerlands.clans.Instanced;
import ru.powerlands.clans.interfaces.Manager;
import ru.powerlands.clans.manager.LeaderBordManager;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.util.MultiUtil;

import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
public class PlaceholderManager extends Instanced implements Manager {

    private Placeholder placeholder;
    public PlaceholderManager(ClanPlugin plugin) {
        super(plugin);
    }


    @Override
    public void load() {
        placeholder = new Placeholder(getPlugin());
        placeholder.register();
    }

    @Override
    public void unload() {
        placeholder.unregister();
    }

    public static class Placeholder extends PlaceholderExpansion {

        private final ClanPlugin clanPlugin;
        private final ManagerService managerService;
        public Placeholder(ClanPlugin clanPlugin) {
            this.clanPlugin = clanPlugin;
            this.managerService = clanPlugin.getManagerService();
        }

        public final Cache<Player, String> waitAccept = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();

        public @Nullable String onPlaceholderRequest(Player player, @NotNull String params) {
            if(params.equals("name")) {
                if(waitAccept.getIfPresent(player) != null) {
                    return waitAccept.getIfPresent(player);
                }
                String format = clanPlugin.getManagerService().getConfigManager().getMessages().getString("placeholder");
                String rawFormat = getDisplayname(player);
                if(!rawFormat.isEmpty()) {
                    format = rawFormat;
                }
                assert format != null;
                waitAccept.put(player, format);
                return format;
            }
            if(params.equals("name_format")) {
                String format = "";
                String rawFormat = getDisplayname(player);
                if(!rawFormat.isEmpty()) {
                    format = rawFormat;
                }
                if(!format.isEmpty()) {
                    return "§7<" + format + "§7> ";
                }
                return "";
            }
            if(params.equals("name_format_no")) {
                return getDisplayname(player);
            }
            LeaderBordManager leaderBordManager = managerService.getLeaderBordManager();
            if (params.equalsIgnoreCase("timeUpdate"))
                return Messager.getRuTimeForm(leaderBordManager.getSeconds());
            if (params.startsWith("topBalance_clan_")) {
                int top = Integer.parseInt(params.replace("topBalance_clan_", "")) - 1;
                if (leaderBordManager.getTopBalance().containsKey(top))
                    return (leaderBordManager.getTopBalance().get(top)).getDisplayName();
                return managerService.getConfigManager().getMessages().getString("absent");

            }
            if (params.startsWith("topBalance_you")) {
                ClanPlayer clanPlayer = managerService.getClanManager().getClanPlayer(player.getName());
                if(clanPlayer != null) {
                    Clan clan = managerService.getClanManager().getClanByTag(clanPlayer.getClanTag());
                    return String.valueOf(MultiUtil.format(leaderBordManager.getClanPosition(clan)));
                }
            }
            if (params.startsWith("topBalance_player_")) {
                int top = Integer.parseInt(params.replace("topBalance_player_", "")) - 1;
                if (leaderBordManager.getTopBalance().containsKey(top))
                    return managerService.getClanManager().getOwner(leaderBordManager.getTopBalance().get(top)).getPlayer();
                return managerService.getConfigManager().getMessages().getString("absent");
            }
            if (params.startsWith("topBalance_size_")) {
                int top = Integer.parseInt(params.replace("topBalance_size_", "")) - 1;
                if (leaderBordManager.getTopBalance().containsKey(top))
                    return String.valueOf(MultiUtil.format(leaderBordManager.getTopBalance().get(top).getRating()));
                return managerService.getConfigManager().getMessages().getString("absent");

            }
            return null;
        }

        public String getDisplayname(Player player) {
            String format = "";
            ClanPlayer clanPlayer = managerService.getClanManager().getClanPlayer(player.getName());
            if(clanPlayer != null) {
                Clan clan = managerService.getClanManager().getClanByTag(clanPlayer.getClanTag());
                if (clan != null) {
                    format = clan.getDisplayName();
                }
            }
            return format;
        }
        @Override
        public @NotNull String getIdentifier() {
            return "landsclans";
        }

        @Override
        public @NotNull String getAuthor() {
            return "Power_Lands";
        }

        @Override
        public @NotNull String getVersion() {
            return "2.0";
        }
    }

}