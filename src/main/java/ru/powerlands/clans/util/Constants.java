package ru.powerlands.clans.util;

public class Constants {

    public static final int[] MAIN_MENU_PANELS = {0, 1, 2, 3, 4, 5, 6, 7, 8, 17, 26, 35, 9, 18, 27, 36, 37, 38, 39, 40, 41, 42, 43, 44};
    public static final int[] MEMBER_MENU_PANELS = {0, 1, 2, 3, 4, 5, 6, 7, 8, 17, 35, 9, 27, 36, 37, 38, 39, 40, 41, 42, 43, 44};
    public static final int[] STORAGE_MENU_PANELS = {0, 1, 2, 3, 4, 5, 6, 7, 8, 17, 26, 35, 9, 18, 27, 36, 37, 38, 39, 40, 41, 42, 43, 44};
    public static final int[] REWARD_MENU_PANELS = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 46, 47, 48, 49, 50, 51, 52};
}
