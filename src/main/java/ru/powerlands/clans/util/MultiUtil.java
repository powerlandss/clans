package ru.powerlands.clans.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Location;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import ru.powerlands.clans.ClanPlugin;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author phaed
 */

public final class MultiUtil {

    private static final Gson GSON = new Gson();
    private static final Type RESIGN_TYPE = TypeToken.getParameterized(Map.class, String.class, Long.class).getType();
    private MultiUtil() {
    }

    @Contract("null -> null")
    @Nullable
    public static String toLanguageTag(@Nullable Locale locale) {
        return locale != null ? locale.toLanguageTag() : null;
    }

    public static Set<Path> getPathsIn(String path, Predicate<? super Path> filter) {
        Set<Path> files = new LinkedHashSet<>();
        String packagePath = path.replace(".", "/");

        try {
            URI uri = ClanPlugin.class.getProtectionDomain().getCodeSource().getLocation().toURI();
            FileSystem fileSystem = FileSystems.newFileSystem(URI.create("jar:" + uri), Collections.emptyMap());
            files = Files.walk(fileSystem.getPath(packagePath)).
                    filter(Objects::nonNull).
                    filter(filter).
                    collect(Collectors.toSet());
            fileSystem.close();
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger("MultiTool").
                    log(Level.WARNING, "An error occurred while trying to load files: " + ex.getMessage(), ex);
        }

        return files;
    }

    public static Set<Class<?>> getClasses(String packageName) {
        Set<Class<?>> classes = new LinkedHashSet<>();

        Predicate<? super Path> filter = entry -> {
            String path = entry.getFileName().toString();
            return !path.contains("$") && path.endsWith(".class");
        };

        for (Path filesPath : getPathsIn(packageName, filter)) {
            // Compatibility with different Java versions
            String path = filesPath.toString();
            if (path.charAt(0) == '/') {
                path = path.substring(1);
            }

            String fileName = path.replace("/", ".").split(".class")[0];

            try {
                Class<?> clazz = Class.forName(fileName);
                classes.add(clazz);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return classes;
    }

    @SuppressWarnings("unchecked")
    public static <T> Set<Class<? extends T>> getSubTypesOf(String packageName, Class<?> type) {
        return getClasses(packageName).stream().
                filter(type::isAssignableFrom).
                map(aClass -> ((Class<? extends T>) aClass)).
                collect(Collectors.toSet());
    }

    /**
     * Parses the default rank from the specified Json String
     *
     * @param json the Json String
     * @return the default rank or null if not found and/or it does not exist
     */
    public static @Nullable String defaultRankFromJson(@Nullable String json) {
        if (json != null && !json.isEmpty()) {
            JsonObject object = GSON.fromJson(json, JsonObject.class);
            JsonElement defaultRank = object.get("defaultRank");
            if (defaultRank != null && !defaultRank.isJsonNull()) {
                return defaultRank.getAsString();
            }
        }
        return null;
    }

    /**
     * Converts a resign times map to a JSON String
     *
     * @param resignTimes the resign times
     * @return a JSON String
     */
    public static String resignTimesToJson(Map<String, Long> resignTimes) {
        return GSON.toJson(resignTimes);
    }

    /**
     * Converts a JSON String to a resign times map
     *
     * @param json JSON String
     * @return a map
     */
    public static @Nullable Map<String, Long> resignTimesFromJson(String json) {
        if (json != null && !json.isEmpty()) {
            return GSON.fromJson(json, RESIGN_TYPE);
        }
        return null;
    }

    public static String removeColorCodes(String input) {
        Pattern pattern = Pattern.compile("&[0-9a-fA-Fk-oK-OrR]");
        Matcher matcher = pattern.matcher(input);
        String result = matcher.replaceAll("");
        result = result.replaceAll("&#([0-9a-fA-F]{6})", "");
        return result;
    }

    /**
     * Returns the delay in seconds to the specified hour and minute.
     *
     * @param hour   hour
     * @param minute minute
     * @return the delay in seconds
     */
    public static long getDelayTo(int hour, int minute) {
        if (hour < 0 || hour > 23) hour = 1;
        if (minute < 0 || minute > 59) minute = 0;
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime d = LocalDateTime.of(now.toLocalDate(), LocalTime.of(hour, minute));
        long delay;
        if (now.isAfter(d)) {
            delay = now.until(d.plusDays(1), ChronoUnit.SECONDS);
        } else {
            delay = now.until(d, ChronoUnit.SECONDS);
        }
        return delay;
    }

    /**
     * Converts string array to {@literal ArrayList<String>}, remove empty strings
     *
     * @param values
     * @return
     */
    public static List<String> fromArrayToList(String... values) {
        List<String> results = new ArrayList<>();
        Collections.addAll(results, values);
        results.remove("");
        return results;
    }

    /**
     * Converts string array to {@literal HashSet<String>}, remove empty strings
     *
     * @param values
     * @return
     */
    @Deprecated
    public static Set<String> fromArrayToSet(String... values) {
        HashSet<String> results = new HashSet<>();
        Collections.addAll(results, values);
        results.remove("");
        return results;
    }

    /**
     * Converts  {@literal ArrayList<String>} to string array
     *
     * @param list
     * @return
     */
    public static String[] toArray(List<String> list) {
        return list.toArray(new String[0]);
    }

    /**
     * Generates page separator line
     *
     * @param sep
     * @return
     */
    public static String generatePageSeparator(String sep) {
        String out = "";

        for (int i = 0; i < 320; i++) {
            out += sep;
        }
        return out;
    }

    /**
     * Escapes single quotes
     *
     * @param str
     * @return
     */
    public static String escapeQuotes(@Nullable String str) {
        if (str == null) {
            return "";
        }
        return str.replace("'", "''");
    }

    /**
     * Returns a prettier coordinate
     *
     * @param loc
     * @return
     */
    public static String toLocationString(Location loc) {
        return loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ() + " " + loc.getWorld().getName();
    }

    /**
     * Sorts a Map by value
     *
     * @param map the Map to sort
     * @return the Map sorted
     */
    public static <K, V extends Comparable<V>> Map<K, V> sortByValue(Map<K, V> map) {
        LinkedList<Map.Entry<K, V>> entryList = new LinkedList<>(map.entrySet());
        entryList.sort(Entry.comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Entry<K, V> entry : entryList) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    /**
     * Formats max inactive days to an infinity symbol if it's negative or 0
     *
     * @param max inactive days
     * @return formatted message
     */
    public static String formatMaxInactiveDays(int max) {
        if (max <= 0) {
            return "∞";
        } else {
            return String.valueOf(max);
        }
    }
    public static String format(long number) {
        if (number < 1000) {
            return String.valueOf(number);
        } else {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
            symbols.setGroupingSeparator('.');
            DecimalFormat formatter = new DecimalFormat("#,###.###", symbols);
            return formatter.format(number);
        }
    }
}