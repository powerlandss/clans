package ru.powerlands.clans.util.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.LongStringType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.powerlands.clans.model.Clan;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Permission;
import ru.powerlands.clans.model.Status;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class ClanPlayerPersister extends LongStringType {

    private static final ClanPlayerPersister singleton = new ClanPlayerPersister();

    public static ClanPlayerPersister getSingleton() {
        return singleton;
    }

    public ClanPlayerPersister() {
        super(SqlType.LONG_STRING, new Class<?>[]{ClanPlayerPersister.class});
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        if(javaObject instanceof List<?>) {
            List<ClanPlayer> clanPlayers = (List<ClanPlayer>) javaObject;
            JSONArray value = new JSONArray();
            for(ClanPlayer clanPlayer : clanPlayers) {
                JSONObject rawClanPlayerObject = new JSONObject();
                try {
                    rawClanPlayerObject.put("name", clanPlayer.getPlayer());
                    rawClanPlayerObject.put("status", clanPlayer.getStatus());
                    rawClanPlayerObject.put("permissions", serializeToJson(clanPlayer.getPermissionList()));
                    value.put(rawClanPlayerObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return value.toString();
        }
        return null;
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(sqlArg.toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        List<ClanPlayer> clanPlayers = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            ClanPlayer clanPlayer = new ClanPlayer();
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                clanPlayer.setPlayer(jsonObject.getString("name"));
                clanPlayer.setStatus(Status.valueOf(jsonObject.getString("status")));
                clanPlayer.setPermissionList(deserializeFromJson(jsonObject.getJSONArray("permissions")));
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            clanPlayers.add(clanPlayer);
        }
        return clanPlayers;
    }

    public static JSONArray serializeToJson(List<Permission> permissionList) {
        JSONArray jsonArray = new JSONArray();
        for (Permission permission : permissionList) {
            JSONObject jsonPermission = new JSONObject();
            try {
                jsonPermission.put("name", permission.name());
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            jsonArray.put(jsonPermission);
        }
        return jsonArray;
    }

    public static List<Permission> deserializeFromJson(JSONArray jsonArray) {
        List<Permission> permissionList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonPermission = null;
            try {
                jsonPermission = (JSONObject) jsonArray.get(i);
                String permissionName = (String) jsonPermission.get("name");
                Permission permission = Permission.valueOf(permissionName.toUpperCase());
                permissionList.add(permission);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
        return permissionList;
    }
}
