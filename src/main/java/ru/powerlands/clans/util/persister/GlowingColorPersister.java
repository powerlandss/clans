package ru.powerlands.clans.util.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.LongStringType;
import org.json.JSONException;
import org.json.JSONObject;
import ru.powerlands.clans.model.GlowingColor;

import java.sql.SQLException;

public class GlowingColorPersister extends LongStringType {
    private static final GlowingColorPersister singleton = new GlowingColorPersister();

    public static GlowingColorPersister getSingleton() {
        return singleton;
    }

    public GlowingColorPersister() {
        super(SqlType.LONG_STRING, new Class<?>[]{GlowingColorPersister.class});
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        GlowingColor glowingColor = (GlowingColor) javaObject;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("red", glowingColor.getRed());
            jsonObject.put("green", glowingColor.getGreen());
            jsonObject.put("blue", glowingColor.getBlue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(sqlArg.toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        GlowingColor glowingColor = new GlowingColor();
        try {
            glowingColor.setRed(jsonObject.getInt("red"));
            glowingColor.setGreen(jsonObject.getInt("green"));
            glowingColor.setBlue(jsonObject.getInt("blue"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return glowingColor;
    }
}