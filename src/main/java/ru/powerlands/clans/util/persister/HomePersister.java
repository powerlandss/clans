package ru.powerlands.clans.util.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.LongStringType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.powerlands.clans.model.Home;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HomePersister extends LongStringType {
    private static final HomePersister singleton = new HomePersister();

    public static HomePersister getSingleton() {
        return singleton;
    }

    public HomePersister() {
        super(SqlType.LONG_STRING, new Class<?>[]{List.class});
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        if(javaObject instanceof List<?>) {
            List<Home> homes = (List<Home>) javaObject;
            JSONArray value = new JSONArray();
            for(Home home : homes) {
                JSONObject rawClanPlayerObject = new JSONObject();
                try {
                    rawClanPlayerObject.put("name", home.getName());
                    rawClanPlayerObject.put("world", home.getWorld());
                    rawClanPlayerObject.put("location", locationToString(home.getLocation()));
                    value.put(rawClanPlayerObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return value.toString();
        }
        return null;
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(sqlArg.toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        List<Home> homes = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            Home home = new Home();
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                home.setName(jsonObject.getString("name"));
                home.setWorld(jsonObject.getString("world"));
                home.setLocation(stringToLocation(jsonObject.getString("location")));
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            homes.add(home);
        }
        return homes;
    }

    private String locationToString(Location location) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("world", location.getWorld().getName());
            jsonObject.put("x", location.getX());
            jsonObject.put("y", location.getY());
            jsonObject.put("z", location.getZ());
            jsonObject.put("yaw", location.getYaw());
            jsonObject.put("pitch", location.getPitch());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
    private Location stringToLocation(String rawLocation) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(rawLocation);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Location location = null;
        try {
            String world = jsonObject.getString("world");
            double x = jsonObject.getDouble("x");
            double y = jsonObject.getDouble("y");
            double z = jsonObject.getDouble("z");
            float yaw = jsonObject.getInt("yaw");
            float pitch = jsonObject.getInt("pitch");
            location = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }
}
