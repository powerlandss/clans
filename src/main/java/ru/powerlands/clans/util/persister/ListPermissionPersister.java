package ru.powerlands.clans.util.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.LongStringType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.powerlands.clans.model.ClanPlayer;
import ru.powerlands.clans.model.Permission;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ListPermissionPersister extends LongStringType {
    private static final ListPermissionPersister singleton = new ListPermissionPersister();

    public static ListPermissionPersister getSingleton() {
        return singleton;
    }

    public ListPermissionPersister() {
        super(SqlType.LONG_STRING, new Class<?>[]{List.class});
    }
    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        if(javaObject instanceof List<?>) {
            List<Permission> permissionList = (List<Permission>) javaObject;
            return serializePermissions(permissionList);
        }
        return null;
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
        return deserializePermissions(sqlArg.toString());
    }

    public static String serializePermissions(List<Permission> permissions) {
        JSONArray jsonArray = new JSONArray();
        for (Permission permission : permissions) {
            jsonArray.put(permission.name());
        }
        return jsonArray.toString();
    }

    public static List<Permission> deserializePermissions(String json) {
        List<Permission> permissions = new ArrayList<>();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(json);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            String permissionName = null;
            try {
                permissionName = jsonArray.getString(i);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            Permission permission = Permission.valueOf(permissionName);
            permissions.add(permission);
        }
        return permissions;
    }
}
