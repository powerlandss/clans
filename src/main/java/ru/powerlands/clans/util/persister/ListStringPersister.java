package ru.powerlands.clans.util.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.LongStringType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ListStringPersister extends LongStringType {

    private static final ListStringPersister singleton = new ListStringPersister();

    public static ListStringPersister getSingleton() {
        return singleton;
    }

    public ListStringPersister() {
        super(SqlType.LONG_STRING, new Class<?>[]{List.class});
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(sqlArg.toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                strings.add(jsonArray.getString(i));
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
        return strings;
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        List<String> objects = (List<String>) javaObject;
        JSONArray jsonArray = new JSONArray();
        for(String str : objects) {
            jsonArray.put(str);
        }
        return jsonArray.toString();
    }
}