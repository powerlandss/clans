package ru.powerlands.clans.util.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.LongStringType;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.powerlands.clans.model.Storage;

import java.sql.SQLException;
import java.util.HashMap;

public class StoragePersister extends LongStringType {

    private static final StoragePersister singleton = new StoragePersister();

    public static StoragePersister getSingleton() {
        return singleton;
    }

    public StoragePersister() {
        super(SqlType.LONG_STRING, new Class<?>[]{Storage.class});
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) throws SQLException {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(sqlArg.toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        Storage storage = new Storage();
        try {
            storage.setClanTag(jsonObject.getString("tag"));
            JSONArray array = jsonObject.getJSONArray("items");
            HashMap<Integer, ItemStack> itemStackHashMap = new HashMap<>();
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                YamlConfiguration yamlConfiguration = new YamlConfiguration();
                yamlConfiguration.loadFromString(object.getString("item"));
                itemStackHashMap.put(object.getInt("slot"), yamlConfiguration.getItemStack("item"));
            }
            storage.getItemStackHashMap().putAll(itemStackHashMap);
        } catch (JSONException | InvalidConfigurationException e) {
            throw new RuntimeException(e);
        }
        return storage;
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        JSONObject jsonObject = new JSONObject();
        Storage storage = (Storage) javaObject;
        try {
            jsonObject.put("tag", storage.getClanTag());
            JSONArray array = new JSONArray();

            for (Integer i : storage.getItemStackHashMap().keySet()) {
                JSONObject itemObject = new JSONObject();
                YamlConfiguration yamlConfiguration = new YamlConfiguration();
                yamlConfiguration.set("item", storage.getItemStackHashMap().get(i));
                itemObject.put("item", yamlConfiguration.saveToString());
                itemObject.put("slot", i);
                array.put(itemObject);
            }
            jsonObject.put("items", array);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}